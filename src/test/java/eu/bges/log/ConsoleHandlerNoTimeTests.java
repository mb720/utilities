package eu.bges.log;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.logging.LogRecord;
import java.util.regex.Pattern;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardErrorStreamLog;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;

import eu.bges.TimeUtil;
import eu.bges.log.ConsoleHandlerNoTime;

/**
 * Tests the {@link ConsoleHandlerNoTime}.
 * 
 * @author Matthias Braun
 * 
 */
public class ConsoleHandlerNoTimeTests {

    /**
     * Contains what has been written to out stream
     */
    @Rule
    public final StandardOutputStreamLog outLog = new StandardOutputStreamLog();
    /**
     * Contains what has been written to error stream
     */
    @Rule
    public final StandardErrorStreamLog errLog = new StandardErrorStreamLog();

    @Test
    public void logToErrContainsExceptionMsg() {
        final String thisMethodsName = "logToErrContainsExceptionMsg";
        final String exceptionMsg = "the exception message";
        final Exception exception = new Exception(exceptionMsg);
        final String logMsg = "something's gone wrong";
        final LogRecord record = LogTestUtil
                .createSevereRecordMsgWithException(logMsg, exception,
                        getClass(), thisMethodsName);
        new ConsoleHandlerNoTime().publish(record);
        final String errMsg = errLog.getLog();
        final boolean containsExceptionMsg = errMsg.contains(exceptionMsg);
        assertTrue(
                "The lob message should be printed to the standard error stream",
                containsExceptionMsg);
    }

    @Test
    public void logToErrContainsLogMsg() {
        final String thisMethodsName = "logToErrContainsLogMsg";
        final String exceptionMsg = "the exception message";
        final Exception exception = new Exception(exceptionMsg);
        final String logMsg = "something's gone wrong";
        final LogRecord record = LogTestUtil
                .createSevereRecordMsgWithException(logMsg, exception,
                        getClass(), thisMethodsName);
        new ConsoleHandlerNoTime().publish(record);
        final String errMsg = errLog.getLog();
        final boolean containsLogMsg = errMsg.contains(logMsg);

        assertTrue(
                "The lob message should be printed to the standard error stream",
                containsLogMsg);
    }

    @Test
    public void logToStandardOutContainsLinkToLine() {
        final String thisMethodsName = "logToStandardOutContainsLinkToLine";
        final LogRecord record = LogTestUtil.createInfoRecord(
                LogTestUtil.SIMPLE_LOG_MSG, getClass(), thisMethodsName);
        new ConsoleHandlerNoTime().publish(record);
        // Assumes that this class and the file it lives in have the same
        // name
        final String fileName = getClass().getSimpleName();
        final String linkRegex = "\\(" + fileName + "\\.java:\\d+\\) ";
        final String logMsg = outLog.getLog();
        final Pattern p = Pattern.compile(linkRegex);
        final boolean containsLink = p.matcher(logMsg).find();
        assertTrue(
                "A link to the location of the log message should be printed to standard out",
                containsLink);
    }

    @Test
    public void logToStandardOutContainsMsg() {
        final String thisMethodsName = "logToStandardOutContainsMsg";
        final String msg = LogTestUtil.SIMPLE_LOG_MSG;
        final LogRecord record = LogTestUtil.createInfoRecord(msg, getClass(),
                thisMethodsName);
        new ConsoleHandlerNoTime().publish(record);
        final boolean containsMsg = outLog.getLog().contains(msg);
        assertTrue("The log message should be printed to standard out",
                containsMsg);
    }

    @Test
    public void logToStandardOutDoesNotContainLocalTime() {
        final String thisMethodsName = "logToStandardOutDoesNotContainLocalTime";
        final LogRecord record = LogTestUtil.createInfoRecord(
                LogTestUtil.SIMPLE_LOG_MSG, getClass(), thisMethodsName);
        new ConsoleHandlerNoTime().publish(record);
        final String dateAsStr = TimeUtil.toLocal(System.currentTimeMillis());
        final boolean containsLocalTime = outLog.getLog().contains(dateAsStr);
        assertFalse(
                "The local time of the log message should not be printed to standard out",
                containsLocalTime);
    }

    @Test
    public void publishingNullGivesErrorMsg() {
        new ConsoleHandlerNoTime().publish(null);
        assertFalse(errLog.getLog().isEmpty());
    }
}
