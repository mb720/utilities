package eu.bges.log;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Provides methods common to testing the log handlers.
 * 
 * @author Matthias Braun
 * 
 */
public class LogTestUtil {
    static final String SIMPLE_LOG_MSG = "my test message";

    /**
     * Creates a {@link LogRecord} that can be published by a log
     * {@link Handler}.
     * 
     * @param msg
     *            the message that goes into the record
     * @param methodWhoLogs
     *            the name of the method that is logging the {@code msg}. This
     *            is used to avoid the need for reflection
     * @return an initialized {@link LogRecord}
     */
    static LogRecord createInfoRecord(final String msg,
            final Class<?> classWhoLogs, final String methodWhoLogs) {
        final Level level = Level.INFO;
        final LogRecord record = new LogRecord(level, msg);
        record.setSourceClassName(classWhoLogs.getName());
        record.setSourceMethodName(methodWhoLogs);
        return record;
    }

    /**
     * Creates a {@link LogRecord} that contains not only a {@code logMsg} of
     * level 'SEVERE' but also an {@link Exception}.
     * 
     * @param logMsg
     *            the message that goes into the record
     * @param exception
     *            the exception that goes into the record
     * @param methodWhoLogs
     *            the name of the method that is logging the {@code logMsg}.
     *            This is used to avoid the need for reflection
     * @param classWhoLogs
     *            the name of the class that is logging the {@code logMsg}
     * 
     * @return an initialized {@link LogRecord}
     */
    static LogRecord createSevereRecordMsgWithException(final String logMsg,
            final Exception exception, final Class<?> classWhoLogs,
            final String methodWhoLogs) {

        final Level level = Level.SEVERE;
        final LogRecord record = new LogRecord(level, logMsg);
        record.setSourceClassName(classWhoLogs.getName());
        record.setSourceMethodName(methodWhoLogs);
        record.setThrown(exception);
        return record;
    }
}
