package eu.bges;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import eu.bges.IOUtil;
import eu.bges.config.AbstractPropertiesLoader;
import eu.bges.config.KeyNotFoundException;

public class AbstractPropertiesLoaderTests {

	private class InvalidPropertyLoader extends AbstractPropertiesLoader {

		private static final String STRINGS_PROPERTIES = NON_EXISTANT;

		public String get(final String key) throws KeyNotFoundException {
			return getValue(key);
		}

		@Override
		public File getPropertiesFile() {
			final File f = IOUtil.getResource(STRINGS_PROPERTIES, getClass());
			return f;
		}
	}

	private class ValidPropertyLoader extends AbstractPropertiesLoader {

		private static final String STRINGS_PROPERTIES = "/testfiles/strings.properties";

		public String get(final String key) throws KeyNotFoundException {
			return getValue(key);
		}

		@Override
		public File getPropertiesFile() {
			final File f = IOUtil.getResource(STRINGS_PROPERTIES, getClass());
			return f;
		}
	}

	private static final String NON_EXISTANT = "nonExistant";
	private static final String PROPERTY_VALUE = "Copy selected text to clipboard and delete it in the editor";

	private static final String PROPERTY_KEY = "cutDescr";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void getValueFromInvalidLoader() throws KeyNotFoundException {
		final InvalidPropertyLoader loader = new InvalidPropertyLoader();
		final String key = PROPERTY_KEY;

		exception.expect(KeyNotFoundException.class);
		loader.get(key);
	}

	@Test
	public void getValueFromValidLoader() throws KeyNotFoundException {
		final ValidPropertyLoader loader = new ValidPropertyLoader();
		final String key = PROPERTY_KEY;
		final String expectedValue = PROPERTY_VALUE;

		final String value = loader.get(key);
		assertEquals(expectedValue, value);
	}

	@Test
	public void nonExistantKey() throws KeyNotFoundException {
		final ValidPropertyLoader loader = new ValidPropertyLoader();
		final String key = NON_EXISTANT;

		/*
		 * This test expects a KeyNotFoundException to happen after this
		 * statement
		 */
		exception.expect(KeyNotFoundException.class);
		loader.get(key);
	}
}
