package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.junit.Test;
import eu.bges.LocaleUtil.LangTag;
import eu.bges.strings.StringUtil;

public class StringUtilTests {

	/**
	 * "Persian" in Persian to test for non-ascii, right-to-left strings
	 */
	public static final String PERSIAN_STRING = "فارسی";

	@Test
	public void compareAlphabeticallyInAustrian() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.AUSTRIAN);
		compareAlphabetically();

		// Restore the old locale
		Locale.setDefault(oldLocale);
	}

	@Test
	public void compareAlphabeticallyInGerman() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.GERMAN);
		compareAlphabetically();

		// Restore the old locale
		Locale.setDefault(oldLocale);
	}

	@Test
	public void compareAlphabeticallyInTurkish() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.TURKISH);
		compareAlphabetically();

		// Restore the old locale
		Locale.setDefault(oldLocale);
	}

	@Test
	public void compareAlphabeticallyInUsEnglish() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.US);
		compareAlphabetically();

		// Restore the old locale
		Locale.setDefault(oldLocale);
	}

	/**
	 * An uppercase I will be converted to a lowercase <i>ı</i> <b>without the
	 * dot</b> if the default locale is Turkish. That's how string comparisons
	 * that want to ignore case using {@code str.toLowerCase()} suddenly fail if
	 * the locale is Turkish.
	 * <p>
	 * Solution: use {@code str.toLowerCase(Locale.ENGLISH)}
	 *
	 * @see <a href="http://java.sys-con.com/node/46241">Turkish Java Needs
	 *      Special Brewing</a>
	 */
	@Test
	public void confirmTurkishLocaleBug() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.TURKISH);
		final boolean output = "eclipse".equals("ECLIPSE".toLowerCase());
		// Reset the locale
		Locale.setDefault(oldLocale);
		/*
		 * Those strings are not equal because the second one is 'eclıpse' (no
		 * dot above the i)
		 */
		assertFalse(output);
	}

	@Test
	public void containsIgnoreCase() {
		final String input = "eclipse";
		final boolean output = StringUtil.containsIgnoreCase(input, "ECLIPSE");
		assertTrue(output);
	}

	/**
	 *
	 * @see <a href="http://java.sys-con.com/node/46241">Turkish Java Needs
	 *      Special Brewing</a>
	 */
	@Test
	public void containsIgnoreCaseInTurkish() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.TURKISH);
		final String input = "eclipse";
		final boolean output = StringUtil.containsIgnoreCase(input, "ECLIPSE");
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertTrue(output);
	}

	@Test
	public void deleteBetweenEmptyStrings() {
		final String input = "Some inconspicuous string";
		final String actual = StringUtil.deleteBetween(input, "");
		final String expected = input;

		assertEquals(expected, actual);
	}

	@Test
	public void deleteBetweenWithEmptyInput() {
		final String input = "";
		final String actual = StringUtil.deleteBetween(input, "delim");
		final String expected = input;

		assertEquals(expected, actual);
	}

	@Test
	public void deleteBetweenWitValidInput() {
		final String actual = StringUtil.deleteBetween(
				"Remove my 'unnecessary' quotes 'now' please", "'");
		final String expected = "Remove my  quotes  please";
		assertEquals(expected, actual);
	}

	@Test
	public void deleteRange() {
		final String actual = StringUtil.deleteRange("my small example", 3, 8);
		final String expected = "my example";
		assertEquals(expected, actual);
	}

	@Test
	public void distanceWithEmptyInput() {

		final int expected = 0;
		final int actual = StringUtil.getDistance("", "");
		assertEquals(expected, actual);
	}

	@Test
	public void distanceWithValidInput() {

		final int expected = 3;
		final int actual = StringUtil.getDistance("kitten", "sitting");
		assertEquals(expected, actual);
	}

	@Test
	public void getOccurrences() {

		final String input = "*|||*|*";
		final String searchThis = "*";
		final List<Integer> expectedOutput = Arrays.asList(0, 4, 6);
		final List<Integer> output = StringUtil.getOccurrences(searchThis,
				input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void removeLastChar() {
		final String input = "remove my last char!";
		final String expected = "remove my last char";
		final String output = StringUtil.removeLastChar(input);
		assertEquals(expected, output);
	}

	@Test
	public void removeLastCharWithEmptyInput() {
		final String input = "";
		final String expected = "";
		final String output = StringUtil.removeLastChar(input);
		assertEquals(expected, output);
	}

	@Test
	public void removeLastCharWithNullInput() {
		final String input = null;
		final String expected = null;
		final String output = StringUtil.removeLastChar(input);
		assertEquals(expected, output);
	}

	/**
	 *
	 * @see <a href="http://java.sys-con.com/node/46241">Turkish Java Needs
	 *      Special Brewing</a>
	 */
	@Test
	public void replaceIgnoreCaseInTurkish() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.TURKISH);
		final String input = "eclipse is great";
		final String expectedOutput = "ECLIPSE is great";
		final String output = StringUtil.replaceIgnoreCase(input, "eClIpsE",
				"ECLIPSE");
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expectedOutput, output);
	}

	/**
	 *
	 * @see <a href="http://java.sys-con.com/node/46241">Turkish Java Needs
	 *      Special Brewing</a>
	 */
	@Test
	public void replaceIgnoreCaseInTurkishWithNonAsciiInput() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.TURKISH);
		final String input = PERSIAN_STRING;
		final String expectedOutput = PERSIAN_STRING;
		final String output = StringUtil.replaceIgnoreCase(input, "eClIpsE",
				"ECLIPSE");
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void replaceIgnoreCaseWithNonAsciiInput() {
		final String input = PERSIAN_STRING;
		final String expectedOutput = PERSIAN_STRING;
		final String output = StringUtil.replaceIgnoreCase(input, "eClIpsE",
				"ECLIPSE");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void replaceLast() {

		final String input = "Hello";
		final String expectedOutput = "Helo";
		final String output = StringUtil.replaceLast(input, "l", "");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void replaceLastWithBadInput() {

		final String input = "Hello";
		final String expectedOutput = "Hello";
		final String output = StringUtil.replaceLast(input, "xxxx", "");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subAfterTest() {

		final String input = "Hello";
		final String expectedOutput = "lo";
		final String output = StringUtil.subAfter(input, "l");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subAfterTest2() {

		final String input = "Hello";
		final String expectedOutput = "llo";
		final String output = StringUtil.subAfter(input, "He");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subAfterTest3() {

		final String input = "Hello";
		final String expectedOutput = "";
		final String output = StringUtil.subAfter(input, "Hello");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subAfterTest4() {

		final String input = "something.else";
		final String expectedOutput = "else";
		final String output = StringUtil.subAfter(input, ".");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subAfterTestWithBadDelimitor() {

		final String input = "Hello";
		final String expectedOutput = "Hello";
		final String output = StringUtil.subAfter(input, "xxxx");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTest() {

		final String input = "Hello";
		final String expectedOutput = "He";
		final String output = StringUtil.subBefore(input, "l");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTest2() {

		final String input = "Hello";
		final String expectedOutput = "Hel";
		final String output = StringUtil.subBefore(input, "lo");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTest3() {

		final String input = "Hello";
		final String expectedOutput = "";
		final String output = StringUtil.subBefore(input, "Hello");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTest4() {

		final String input = "Hello";
		final String expectedOutput = "H";
		final String output = StringUtil.subBefore(input, "ell");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTest5() {

		final String input = "something.else";
		final String expectedOutput = "something";
		final String output = StringUtil.subBefore(input, ".");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBeforeTestWithBadDelimitor() {

		final String input = "Hello";
		final String expectedOutput = "Hello";
		final String output = StringUtil.subBefore(input, "xxxx");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBetweenWithNonExistantDelimiters() {

		final String input = "something<i>else</i>entirely";
		final String expectedOutput = input;
		final String output = StringUtil.subBetween(input, "---", "");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void subBetweenWithValidInput() {

		final String input = "something<i>else</i>entirely";
		final String expectedOutput = "else";
		final String output = StringUtil.subBetween(input, "<i>", "</i>");
		assertEquals(expectedOutput, output);
	}

	@Test
	public void toStringsWithArray() {
		final Object[] inputArr = { "first", null, "third" };

		final String firstExpected = "first";
		final String secondExpected = "null";
		final String thirdExpected = "third";
		final List<Object> expectedList = new ArrayList<Object>();
		expectedList.add(firstExpected);
		expectedList.add(secondExpected);
		expectedList.add(thirdExpected);

		final List<String> listOfStrings = StringUtil.toStrings(inputArr);
		assertEquals(expectedList, listOfStrings);

	}

	@Test
	public void toStringsWithList() {
		final List<Object> inputList = new ArrayList<>();
		final String first = "first";
		final String second = null;
		final String third = "third";
		inputList.add(first);
		inputList.add(second);
		inputList.add(third);

		final String firstExpected = "first";
		final String secondExpected = "null";
		final String thirdExpected = "third";
		final List<Object> expectedList = new ArrayList<Object>();
		expectedList.add(firstExpected);
		expectedList.add(secondExpected);
		expectedList.add(thirdExpected);
		final List<String> outputList = StringUtil.toStrings(inputList);
		assertEquals(expectedList, outputList);

	}

	private void compareAlphabetically() {
		int result = StringUtil.alphabeticalCompare('A', 'B');
		// Negative result means the first letter comes first
		assertTrue(result < 0);

		result = StringUtil.alphabeticalCompare('A', 'a');
		assertTrue(result < 0);

		// Negative result means the second letter comes first
		result = StringUtil.alphabeticalCompare('z', 'Y');
		assertTrue(result > 0);

		// Equal result means the letters are the same
		result = StringUtil.alphabeticalCompare('l', 'l');
		assertTrue(result == 0);

		// Non-ASCII letters come always after ASCII ones
		result = StringUtil.alphabeticalCompare('!', 'a');
		assertTrue(result > 0);

		result = StringUtil.alphabeticalCompare(' ', 'A');
		assertTrue(result > 0);

		result = StringUtil.alphabeticalCompare('#', 'A');
		assertTrue(result > 0);

		result = StringUtil.alphabeticalCompare('ü', 'p');
		assertTrue(result > 0);

		// Unicode number determines position when comparing non-ASCII letters
		result = StringUtil.alphabeticalCompare('Ö', 'ö');
		assertTrue(result < 0);

		result = StringUtil.alphabeticalCompare('Ö', 'Ö');
		assertTrue(result == 0);

	}
}