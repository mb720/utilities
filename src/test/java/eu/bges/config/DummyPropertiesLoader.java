package eu.bges.config;

import java.io.File;

import eu.bges.IOUtil;
import eu.bges.config.AbstractPropertiesLoader;

public class DummyPropertiesLoader extends AbstractPropertiesLoader {
    private static final String PROP_PATH = "/testfiles/strings.properties";
    private final File propertiesFile = IOUtil.getResource(PROP_PATH,
            getClass());

    @Override
    public File getPropertiesFile() {
        return propertiesFile;
    }

}
