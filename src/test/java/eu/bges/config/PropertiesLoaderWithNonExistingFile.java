package eu.bges.config;

import java.io.File;

import eu.bges.config.AbstractPropertiesLoader;

public class PropertiesLoaderWithNonExistingFile extends
        AbstractPropertiesLoader {

    @Override
    public File getPropertiesFile() {
        return new File("");
    }

}
