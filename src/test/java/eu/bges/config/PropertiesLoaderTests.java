package eu.bges.config;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import eu.bges.config.KeyNotFoundException;

public class PropertiesLoaderTests {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void getExistingListProperty() throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        final String existingKey = "testList";
        final List<String> expectedValues = Arrays.asList("value1", "value2",
                "value3");
        final List<String> actual = loader.getList(existingKey);
        assertEquals(expectedValues, actual);
    }

    @Test
    public void getExistingListPropertyWithCustomSeparator()
            throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        final String existingKey = "testListWithCustomSeparator";
        final List<String> expectedValues = Arrays.asList("value4", "value5",
                "value6");
        final String separator = "|";
        final List<String> actual = loader.getList(existingKey, separator);
        assertEquals(expectedValues, actual);
    }

    @Test
    public void getExistingProperty() throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        final String existingKey = "save";
        final String expectedValue = "Save";
        final String actual = loader.getValue(existingKey);
        assertEquals(expectedValue, actual);
    }

    @Test
    public void getValFromNonExistingFile() throws KeyNotFoundException {
        final PropertiesLoaderWithNonExistingFile loader = new PropertiesLoaderWithNonExistingFile();
        final String key = "file does not exist";
        exception.expect(KeyNotFoundException.class);
        loader.getValue(key);
    }

    @Test
    public void throwExceptionForNonExistingKey() throws KeyNotFoundException {

        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        final String nonExistingKey = "";
        exception.expect(KeyNotFoundException.class);
        loader.getValue(nonExistingKey);
    }

    @Test
    public void throwExceptionForNonExistingListProperty()
            throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        final String nonExistingKey = "";
        exception.expect(KeyNotFoundException.class);
        loader.getList(nonExistingKey);
    }

    @Test
    public void throwExceptionForNullListProperty() throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        exception.expect(NullPointerException.class);
        loader.getList(null);
    }

    @Test
    public void throwExceptionForNullProperty() throws KeyNotFoundException {
        final DummyPropertiesLoader loader = new DummyPropertiesLoader();
        exception.expect(NullPointerException.class);
        loader.getValue(null);
    }
}
