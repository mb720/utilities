package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import eu.bges.LocaleUtil.LangTag;

public strictfp class MathUtilTests {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	/**
	 * Note: This method buggy due to double's limited precision. Don't use
	 * double, use BigDecimal instead.
	 * <p>
	 * Converts a double value to its percent representation.
	 * <p>
	 * The resulting percentage is rounded to three decimal places.
	 * <p>
	 * The output is locale-dependent: The decimal separator will vary according
	 * to the current locale of the VM.
	 * <p>
	 * Example for English (UK) locale:
	 *
	 * <pre>
	 * 0.99 → "99%"
	 * 0.053 → "5.3%"
	 * 0.053335 → "5.334%"
	 * </pre>
	 *
	 * @param val
	 *            to be formated as a percent value
	 * @return a percent representation of the given value
	 */
	public static String buggyDoubleToPercent(final double val) {
		final NumberFormat nf = NumberFormat.getPercentInstance();
		// The default rounding mode is HALF_EVEN
		nf.setRoundingMode(RoundingMode.HALF_UP);
		nf.setMaximumFractionDigits(3);
		return nf.format(val);
	}

	/**
	 * Rounds a double by using the 'multiplication and division' way.
	 * <p>
	 * This is buggy due to the imprecision of {@code double}.
	 *
	 * @param value
	 *            is rounded
	 * @param decimalPlace
	 *            the decimal place to which the number is rounded
	 *
	 * @return the rounded value
	 */
	private static double roundWithMultiplicationAndDivision(
			final double value, final int decimalPlace) {
		return Math.round(value * Math.pow(10, decimalPlace))
				/ Math.pow(10, decimalPlace);
	}

	@Test
	public void binToHexInputWithSpaces() {

		final String binaryInput = "1101 0100 0101 1000 1001";
		final String expectedOutput = "D4589";
		final boolean omitLeadingZeros = true;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexInvalidInput() {

		final String binaryInput = "0201";
		final boolean omitLeadingZeros = true;
		exception.expect(NumberFormatException.class);
		MathUtil.binToHex(binaryInput, omitLeadingZeros);
	}

	@Test
	public void binToHexWith1() {

		final String binaryInput = "0001";
		final String expectedOutput = "1";
		final boolean omitLeadingZeros = true;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexWith15() {

		final String binaryInput = "1111";
		final String expectedOutput = "F";
		final boolean omitLeadingZeros = true;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexWith54NoLeadingZerosWithSpaces() {

		// Input contains spaces
		final String binInput = "0 0000 0011 0110";
		// 54 in decimal
		final String expectedHex = "36";
		final boolean omitLeadingZeros = true;
		final String output = MathUtil.binToHex(binInput, omitLeadingZeros);
		assertEquals(expectedHex, output);
	}

	@Test
	public void binToHexWith54WithLeadingZerosWithSpaces() {

		// Input contains spaces
		final String binInput = "0 0000 0011 0110";
		// 54 in decimal
		final String expectedHex = "0036";
		final boolean omitLeadingZeros = false;
		final String output = MathUtil.binToHex(binInput, omitLeadingZeros);
		assertEquals(expectedHex, output);
	}

	@Test
	public void binToHexWithEmptyInput() {

		final String binaryInput = "";

		final boolean omitLeadingZeros = false;
		exception.expect(NumberFormatException.class);
		MathUtil.binToHex(binaryInput, omitLeadingZeros);
	}

	@Test
	public void binToHexWithInvalidInput() {

		final String binaryInput = "2";

		final boolean omitLeadingZeros = false;
		exception.expect(NumberFormatException.class);
		MathUtil.binToHex(binaryInput, omitLeadingZeros);
	}

	@Test
	public void binToHexWithLeadingCompleteNibbleOfZeros() {

		// The number of binary digits is dividable by four
		final String binaryInput = "0000 0001";
		final String expectedOutput = "01";

		final boolean omitLeadingZeros = false;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexWithLeadingIncompleteNibbleZeros() {

		final String binaryInput = "0 0001";
		final String expectedOutput = "01";

		final boolean omitLeadingZeros = false;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexWithNull() {

		final String binaryInput = null;
		final boolean omitLeadingZeros = true;
		exception.expect(NullPointerException.class);
		MathUtil.binToHex(binaryInput, omitLeadingZeros);
	}

	@Test
	public void binToHexWithOneLeadingZero() {

		final String binaryInput = "00011010100010110001001";
		final String expectedOutput = "0D4589";

		final boolean omitLeadingZeros = false;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void binToHexWithTwoLeadingCompleteNibbleOfZeros() {

		final String binaryInput = "0000 0000 0001";
		final String expectedOutput = "001";

		final boolean omitLeadingZeros = false;
		final String output = MathUtil.binToHex(binaryInput, omitLeadingZeros);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void confirmRoundingBugWithDouble() {

		/*
		 * This number is stored as 1.2345349788665771 internally. Thus it is
		 * rounded _down_ by buggyDoubleToPercent.
		 */
		final double input = 1.234535;
		final String correct = "123.454%";
		final String output = buggyDoubleToPercent(input);
		// It's expected that the output is incorrect
		assertNotEquals(correct, output);
	}

	@Test
	public void confirmRoundingBugWithMultiplicationAndDivision() {

		final double roundMe = 1.005;
		final int decPlace = 2;
		final double output = roundWithMultiplicationAndDivision(roundMe,
				decPlace);
		final double correctRoundedValue = 1.01;
		// It is expected that the buggy method produces not the correct output
		assertNotEquals(correctRoundedValue, output, 0);
	}

	@Test
	public void divByNegativeZero() {

		final int dividend = 6;
		final int divisor = -0;
		exception.expect(ArithmeticException.class);
		MathUtil.div(dividend, divisor);
	}

	@Test
	public void divByZero() {

		final int dividend = 6;
		final int divisor = 0;
		exception.expect(ArithmeticException.class);
		MathUtil.div(dividend, divisor);
	}

	@Test
	public void divOneByThree() {
		final int dividend = 1;
		final int divisor = 3;
		final BigDecimal output = MathUtil.div(dividend, divisor);
		// The answer depends on the default scale of MathUtil
		final StringBuilder expectedStr = new StringBuilder("0.");
		for (int i = 0; i < MathUtil.DEFAULT_SCALE; i++) {
			expectedStr.append(3);
		}
		assertEquals(expectedStr.toString(), output.toString());
	}

	@Test
	public void divSixByTwelve() {

		final int dividend = 6;
		final int divisor = 12;
		final BigDecimal output = MathUtil.div(dividend, divisor);
		final BigDecimal expected = BigDecimal.valueOf(0.5);
		assertEquals(expected, output);
	}

	@Test
	public void divTwoByThree() {
		final int dividend = 2;
		final int divisor = 3;
		final BigDecimal output = MathUtil.div(dividend, divisor);
		// The answer depends on the default scale of MathUtil
		final StringBuilder expectedStr = new StringBuilder("0.");
		for (int i = 0; i < MathUtil.DEFAULT_SCALE - 1; i++) {
			expectedStr.append(6);
		}
		// The result is rounded up
		expectedStr.append(7);
		assertEquals(expectedStr.toString(), output.toString());
	}

	@Test
	public void doubleToPercentNoDecimal() {
		final double input = 0.99;
		final String expected = "99%";

		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentOneHundredPercent() {
		final double input = 1;
		final String expected = "100%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentRoundUp() {
		final double input = 1.234585;
		final String expected = "123.459%";
		// This only works if the rounding mode is set to HALF_UP
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInArabicIraq() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.ARABIC_IRAQ);
		final double input = 0.053335;
		// The colon is the decimal separator in Arabic/Iraq
		final String expected = "5.334%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInArabicSaudiArabia() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.ARABIC_SAUDI_ARABIA);
		final double input = 0.053335;
		// The colon is the decimal separator in Arabic/Saudi Arabia
		final String expected = "5.334%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInGerman() {
		final Locale oldLocale = Locale.getDefault();
		Locale.setDefault(Locale.GERMANY);
		final double input = 0.053335;
		// The colon is the decimal separator in German
		final String expected = "5,334%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInRomanian() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.ROMANIAN);
		final double input = 0.053335;
		// The colon is the decimal separator in Romanian
		final String expected = "5,334%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInSwedish() {
		final Locale oldLocale = Locale.getDefault();
		LocaleUtil.setLocaleTo(LangTag.SWEDISH);
		final double input = 0.053335;
		/*
		 * The colon is the decimal separator in Swedish. There's a space
		 * between the last digit and the percent sign.
		 */
		final String expected = "5,334 %";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithFourDecimalsAndRoundingUpInUK() {
		final Locale oldLocale = Locale.getDefault();
		Locale.setDefault(Locale.UK);
		final double input = 0.053335;
		// The dot is the decimal separator in the UK
		final String expected = "5.334%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithOneDecimalInUK() {
		final Locale oldLocale = Locale.getDefault();
		Locale.setDefault(Locale.UK);
		final double input = 0.053;
		final String expected = "5.3%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void doubleToPercentWithThreeDecimalsInUK() {
		final Locale oldLocale = Locale.getDefault();
		Locale.setDefault(Locale.UK);
		final double input = 0.05333;
		final String expected = "5.333%";
		final String output = MathUtil.toPercent(BigDecimal.valueOf(input));
		// Reset the locale
		Locale.setDefault(oldLocale);
		assertEquals(expected, output);
	}

	@Test
	public void falseBooleanToShort() {
		final short expected = 0;
		final short output = MathUtil.booleanToShort(false);
		assertEquals(expected, output);
	}

	@Test
	public void hexToBinInvalidInput() {

		final String hexInput = "G4589";
		final boolean completeNibble = true;
		exception.expect(NumberFormatException.class);
		MathUtil.hexToBin(hexInput, completeNibble);
	}

	@Test
	public void hexToBinValidInput() {

		final String hexInput = "D4589";
		final String expectedBinary = "11010100010110001001";
		final boolean completeNibble = true;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void hexToBinWith10LowerCase() {

		// hexToBin works case-insensitive
		final String hexInput = "a";
		final String expectedBinary = "1010";
		final boolean completeNibble = false;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void hexToBinWith15() {

		final String hexInput = "F";
		final String expectedBinary = "1111";
		final boolean completeNibble = false;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void hexToBinWith1NoCompleteNibble() {

		final String hexInput = "1";
		final String expectedBinary = "1";
		final boolean completeNibble = false;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void hexToBinWith1WithCompleteNibble() {

		final String hexInput = "1";
		final String expectedBinary = "0001";
		final boolean completeNibble = true;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void hexToBinWithEmptyInput() {

		final String hexInput = "";
		final boolean completeNibble = true;
		exception.expect(NumberFormatException.class);
		MathUtil.hexToBin(hexInput, completeNibble);
	}

	@Test
	public void hexToBinWithNull() {

		final String hexInput = null;
		final boolean completeNibble = false;
		exception.expect(NullPointerException.class);
		MathUtil.hexToBin(hexInput, completeNibble);
	}

	@Test
	public void hexToBinWithSpacesInInput() {

		final String hexInput = "7A BC 90 1F";
		final String expectedBinary = "01111010101111001001000000011111";
		final boolean completeNibble = true;
		final String output = MathUtil.hexToBin(hexInput, completeNibble);
		assertEquals(expectedBinary, output);
	}

	@Test
	public void maximum() {
		final int[] numbers = { 0, -1, 3, Integer.MIN_VALUE };
		final int biggestNumber = 5;
		final int output = MathUtil.max(biggestNumber, numbers);
		assertEquals(biggestNumber, output);
	}

	@Test
	public void maximumWithMAX_VALUE() {
		final int biggestNumber = Integer.MAX_VALUE;
		final int[] numbers = { 0, -1, 3, 123901, biggestNumber };
		final int output = MathUtil.max(32, numbers);
		assertEquals(biggestNumber, output);
	}

	@Test
	public void minimum() {
		final int[] numbers = { 0, -1, 3, Integer.MAX_VALUE };
		final int smallestNumber = -234;
		final int output = MathUtil.min(smallestNumber, numbers);
		assertEquals(smallestNumber, output);
	}

	@Test
	public void minimumWithMIN_VALUE() {
		final int smallestNumber = Integer.MIN_VALUE;
		final int[] numbers = { 0, -1, 3, smallestNumber };
		final int output = MathUtil.min(8, numbers);
		assertEquals(smallestNumber, output);
	}

	@Test
	public void parseDecTestSuccess() {
		assertEquals(
				BigDecimal.valueOf(1.0).compareTo(MathUtil.parseDec("1....0")),
				0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1,005,4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1.005.4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1.005,4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1,005.4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1,,,,005.4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.4).compareTo(
						MathUtil.parseDec("1,005,4")), 0);
		assertEquals(
				BigDecimal.valueOf(1005.49).compareTo(
						MathUtil.parseDec("1,005,49")), 0);
		assertEquals(
				BigDecimal.valueOf(10050500.4).compareTo(
						MathUtil.parseDec("1,005,0500,4")), 0);
		assertEquals(
				BigDecimal.valueOf(10050500.4).compareTo(
						MathUtil.parseDec("1.005.0500,4")), 0);

	}

	@Test
	public void parseDecWithEmptyString() {
		final String input = "";
		final BigDecimal expected = MathUtil.UNKNOWN_VAL;
		final BigDecimal actual = MathUtil.parseDec(input);
		assertEquals(expected.compareTo(actual), 0);
	}

	@Test
	public void parseDecWithNullInput() {
		exception.expect(NumberFormatException.class);
		MathUtil.parseDec(null);
	}

	@Test
	public void parseDecWithStringContainingLetters() {
		exception.expect(NumberFormatException.class);
		MathUtil.parseDec("s1000.0");
	}

	@Test
	public void parseDecWithStringContainingSpaces() {
		final String input = "1 000 123";
		final BigDecimal expected = BigDecimal.valueOf(1000123);
		final BigDecimal actual = MathUtil.parseDec(input);
		assertEquals(expected.compareTo(actual), 0);
	}

	@Test
	public void parseLongFromDoubleStr() {

		final String input = "5.212312414213121";
		final long expectedOutput = 5L;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMaximumDoubleStr() {

		final String input = "" + Double.MAX_VALUE;
		final long expectedOutput = (long) Double.MAX_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMaximumIntStr() {

		final String input = "" + Integer.MAX_VALUE;
		final long expectedOutput = Integer.MAX_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMaximumLongStr() {

		final String input = "" + Long.MAX_VALUE;
		final long expectedOutput = Long.MAX_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMinimumDoubleStr() {

		final String input = "" + Double.MIN_VALUE;
		final long expectedOutput = (long) Double.MIN_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMinimumIntStr() {

		final String input = "" + Integer.MIN_VALUE;
		final long expectedOutput = Integer.MIN_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromMinimumLongStr() {

		final String input = "" + Long.MIN_VALUE;
		final long expectedOutput = Long.MIN_VALUE;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromNegativeIntStr() {

		final String input = "-1";
		final long expectedOutput = -1L;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongFromZerosStr() {

		final String input = "00000000000000";
		final long expectedOutput = 0;
		final long output = MathUtil.parseToLong(input);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parseLongWithNull() {

		final String input = null;
		// An exception is expected to occur after this statement
		exception.expect(NullPointerException.class);
		MathUtil.parseToLong(input);
	}

	@Test
	public void parseLongWithStrThatIsNoNumber() {

		final String input = "j02ifj020asdf";
		// An exception is expected to occur after this statement
		exception.expect(NumberFormatException.class);
		MathUtil.parseToLong(input);
	}

	@Test
	public void roundWithScale1() {

		final double roundMe = 1.46;
		final BigDecimal expected = BigDecimal.valueOf(1.5);
		final int scale = 1;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale10() {

		// final double roundMe = 0.50594724995;
		final double roundMe = 0.00000000009;
		final int scale = 10;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		final BigDecimal expected = BigDecimal.valueOf(0.0000000001);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale15() {

		final double roundMe = 2.5059472499583409;
		final BigDecimal expected = BigDecimal.valueOf(2.5059472499583410);
		final int scale = 15;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale17() {

		final double roundMe = 2.505947249958340915;
		final BigDecimal expected = BigDecimal.valueOf(2.50594724995834092);
		final int scale = 17;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale2() {

		final double roundMe = 1.005;
		final BigDecimal expected = BigDecimal.valueOf(1.01);
		final int scale = 2;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale2Again() {
		final double roundMe = 265.335;
		final BigDecimal expected = BigDecimal.valueOf(265.34);
		final int scale = 2;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale3() {

		final double roundMe = 0.0009434;
		final BigDecimal expected = BigDecimal.valueOf(0.001);
		final int scale = 3;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void roundWithScale3Again() {

		final double roundMe = 123.0054;
		final BigDecimal expected = BigDecimal.valueOf(123.005);
		final int scale = 3;
		final BigDecimal output = MathUtil.round(roundMe, scale);
		// Compare values, ignore scale
		assertEquals(expected.compareTo(output), 0);
	}

	@Test
	public void shortToBooleanFalse() {
		final short falseShort = 0;
		final boolean output = MathUtil.shortToBoolean(falseShort);
		assertFalse(output);
	}

	@Test
	public void shortToBooleanInvalidInput() {
		final short invalidShort = -1;
		exception.expect(IllegalArgumentException.class);
		MathUtil.shortToBoolean(invalidShort);
	}

	@Test
	public void shortToBooleanTrue() {
		final short trueShort = 1;
		final boolean output = MathUtil.shortToBoolean(trueShort);
		assertTrue(output);
	}

	@Test
	public void toPaddedHex() {

		final int input = 10;
		final String expectedOutput = "0A";
		final String output = MathUtil.decToPaddedHex(input, 2);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void toPaddedHexWith16DigitsMinimum() {

		final int input = 123;
		final String expectedOutput = "000000000000007B";
		final String output = MathUtil.decToPaddedHex(input, 16);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void toPaddedHexWithNegativeDecNumber() {

		final int input = -234;
		final String expectedOutput = "FFFFFFFFFFFFFF16";
		final String output = MathUtil.decToPaddedHex(input, 0);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void toPaddedHexWithNegativeMinNumber() {

		final int input = 996;
		final String expectedOutput = "3E4";
		final String output = MathUtil.decToPaddedHex(input, -2);
		assertEquals(expectedOutput, output);
	}

	@Test
	public void trueBooleanToShort() {
		final short expected = 1;
		final short output = MathUtil.booleanToShort(true);
		assertEquals(expected, output);
	}
}
