package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.google.common.base.Optional;

import eu.bges.ReflectionUtil;

public class ReflectionUtilTests {
	@Test
	public void getNameOfThisClass() {
		final Optional<StackTraceElement> callerMaybe = ReflectionUtil
				.getCallerStackFrame(getClass().getName());
		if (callerMaybe.isPresent()) {
			final StackTraceElement caller = callerMaybe.get();
			assertEquals(
					"ReflectionUtil should get the fully qualified name of this class",
					getClass().getName(), caller.getClassName());

		} else {
			fail("Could not get caller");
		}
	}

	@Test
	public void getThreadName() {
		final long threadId = Thread.currentThread().getId();
		final String expectedThreadName = Thread.currentThread().getName();
		final Optional<Thread> threadMaybe = ReflectionUtil.getThread(threadId);
		if (threadMaybe.isPresent()) {
			final String threadName = threadMaybe.get().getName();
			assertEquals("ReflectionUtil has returned the wrong thread",
					expectedThreadName, threadName);
		} else {
			fail("Could not find the current thread by its ID");
		}

	}
}
