package eu.bges;

import static org.junit.Assert.assertEquals;

import java.util.Currency;
import java.util.Locale;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.bges.LocaleUtil.LangTag;

public class FinanceUtilTests {
    private static final Logger LOG = LoggerFactory
            .getLogger(FinanceUtilTests.class);

    @Test
    public void formatToCurrencyEuroAustria() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.AUSTRIAN);
        final String euroSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 200_000_000.0505;
        /*
         * Comma is the decimal separator in Austria; currency symbol comes
         * before the amount, with one space. The dot is the digit group
         * separator.
         */
        final String expected = euroSymbol + " 200.000.000,051";
        final String output = FinanceUtil.doubleToCurrency(input);
        LOG.info(output);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

    @Test
    public void formatToCurrencyEuroGermany() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.GERMAN);
        final String euroSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 200_000_000.0505;
        /*
         * Comma is the decimal separator in Germany; currency symbol comes
         * after the amount, with one space. The dot is the digit group
         * separator.
         */
        final String expected = "200.000.000,051 " + euroSymbol;
        final String output = FinanceUtil.doubleToCurrency(input);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

    @Test
    public void formatToCurrencyEuroSlovenia() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.SLOVENIAN);
        final String euroSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 200_000_000.0505;
        /*
         * Comma is the decimal separator in Slovenia; currency symbol comes
         * before the amount, with one space. The dot is the digit group
         * separator.
         */
        final String expected = euroSymbol + " 200.000.000,051";
        final String output = FinanceUtil.doubleToCurrency(input);
        LOG.info(output);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

    @Test
    public void formatToCurrencyPoundUK() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.UK);
        final String poundSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 200_000_000.0505;
        /*
         * Dot is the decimal separator in the UK; currency symbol comes before
         * the amount, no space. The comma is the digit group separator.
         */
        final String expected = poundSymbol + "200,000,000.051";
        final String output = FinanceUtil.doubleToCurrency(input);
        LOG.info(output);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

    @Test
    public void formatToCurrencyPoundUS() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.US);
        final String dollarSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 200_000_000.0505;
        /*
         * Dot is the decimal separator in the US; currency symbol comes before
         * the amount, no space. The comma is the digit group separator.
         */
        final String expected = dollarSymbol + "200,000,000.051";
        final String output = FinanceUtil.doubleToCurrency(input);
        LOG.info(output);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

    @Test
    public void formatToCurrencySwedishKrona() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.SWEDISH);
        final String kronaSymbol = Currency.getInstance(Locale.getDefault())
                .getSymbol();

        final double input = 0.0555;
        /*
         * Comma is the decimal separator in Swedish; currency symbol comes
         * after the amount, with one space
         */
        final String expected = "0,056 " + kronaSymbol;
        final String output = FinanceUtil.doubleToCurrency(input);

        // Reset the locale
        Locale.setDefault(oldLocale);
        assertEquals(expected, output);
    }

}
