package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.graphics.RGB;
import org.junit.Test;

import com.google.common.base.Optional;

import eu.bges.ColorUtil;

public class ColorUtilTests {
	@Test
	public void recognizePlainTextColor() {
		final String green = "green";
		boolean greenIsAPlaintextColor = ColorUtil.isPlaintextColor(green);
		assertTrue("ColorUtil doesn't recognize " + green
				+ " as a plaintext color", greenIsAPlaintextColor);
	}

	@Test
	public void getHexFromPlaintext() {
		final String plainText = "cyan";
		final String expectedHex = "#00FFFF";
		Optional<String> hexMaybe = ColorUtil.anyColorToHex(plainText);
		if (hexMaybe.isPresent()) {
			String hex = hexMaybe.get();
			assertEquals(hex, expectedHex);

		} else {

			fail("Color " + plainText + " is unknown to ColorUtil");
		}
	}

	@Test
	public void getColorNameFromHex() {

		final String hexColor = "#d2691e";
		final String expectedPlaintext = "chocolate";

		List<String> colorNames = ColorUtil.hexToPlain(hexColor);
		if (colorNames.size() == 1) {
			String plainText = colorNames.get(0);
			assertEquals(plainText, expectedPlaintext);
		} else {
			fail("Hex color " + hexColor + " is unknown to ColorUtil");
		}
	}

	@Test
	public void recognizedDifferentColorFormats() {

		final String hexColor = "#d2691f";
		final String plaintext = "Blue";
		final String rgbString = "1, 2, 134";
		final String otherRgbString = "rgb(1,23,144)";
		final RGB rgb = new RGB(2, 41, 19);
		assertTrue(ColorUtil.isColor(hexColor));
		assertTrue(ColorUtil.isColor(plaintext));
		assertTrue(ColorUtil.isColor(rgbString));
		assertTrue(ColorUtil.isColor(otherRgbString));
		assertTrue(ColorUtil.isColor(rgb));
	}

	@Test
	public void dontRecognizedInvalidColors() {

		final String hexColor = "#d2691f1234";
		final String plaintext = "bluexxxxx";
		final String rgbString = "1, 2, -134";
		final String otherRgbString = "rgb(-1,23,144)";
		final String tooManyNumbers = "1;3;4;131";

		assertFalse(ColorUtil.isColor(hexColor));
		assertFalse(ColorUtil.isColor(plaintext));
		assertFalse(ColorUtil.isColor(rgbString));
		assertFalse(ColorUtil.isColor(otherRgbString));
		assertFalse(ColorUtil.isColor(tooManyNumbers));
	}

	/**
	 * As cyan and aqua describe the same color according to <a
	 * href="http://www.w3schools.com/html/html_colornames.asp">this table</a> ,
	 * they should both be returned by {@link ColorUtil#hexToPlain(String)}
	 */
	@Test
	public void getMultipleColorNamesFromHex() {

		final String hexColor = "#00ffff";
		final String[] colors = { "cyan", "aqua" };

		List<String> colorNames = ColorUtil.hexToPlain(hexColor);
		boolean colorsFound = colorNames.containsAll(Arrays.asList(colors));
		assertTrue(colorsFound);
	}

	@Test
	public void parseCommaSeparatedStringToRgb() {
		String containsRgb = "0, 233, 1";
		boolean isRgb = ColorUtil.isRgb(containsRgb);
		assertTrue(isRgb);
		Optional<RGB> rgbMaybe = ColorUtil.rgbStringToRgb(containsRgb);
		assertTrue(rgbMaybe.isPresent());
	}

	@Test
	public void parseSpaceSeparatedStringToRgb() {
		String containsRgb = "0 233 1";
		boolean isRgb = ColorUtil.isRgb(containsRgb);
		assertTrue(isRgb);
		Optional<RGB> rgbMaybe = ColorUtil.rgbStringToRgb(containsRgb);
		assertTrue(rgbMaybe.isPresent());
	}

	@Test
	public void parseSemiColonSeparatedStringToRgb() {
		String containsRgb = "0;233;1";
		boolean isRgb = ColorUtil.isRgb(containsRgb);
		assertTrue(isRgb);
		Optional<RGB> rgbMaybe = ColorUtil.rgbStringToRgb(containsRgb);
		assertTrue(rgbMaybe.isPresent());
	}

	@Test
	public void handleInvalidRgbs() {

		String containsRgb = "0;-233;256";
		boolean isRgb = ColorUtil.isRgb(containsRgb);
		assertFalse(isRgb);
		Optional<RGB> rgbMaybe = ColorUtil.rgbStringToRgb(containsRgb);
		assertFalse(rgbMaybe.isPresent());
	}

	@Test
	public void parseStringRepresentationOfRgb() {

		RGB rgb = new RGB(0, 25, 255);
		Optional<RGB> rgbMaybe = ColorUtil.rgbStringToRgb(rgb.toString());
		assertTrue(rgbMaybe.isPresent());
	}
}
