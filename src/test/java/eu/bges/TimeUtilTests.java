package eu.bges;

import static org.junit.Assert.*;

import org.joda.time.DateTimeZone;
import org.junit.Assert;
import org.junit.Test;

import eu.bges.TimeUtil;

public class TimeUtilTests {

    private final String viennaTimezoneTag = "Europe/Vienna";
    private final String londonTimezoneTag = "Europe/London";
    private final String saoPauloTimezoneTag = "America/Sao_Paulo";
    private final String kathmanduTimezoneTag = "Asia/Kathmandu";

    @Test
    public void isApril31stValid() {
        final int day = 31;
        final int year = 1988;
        final int month = 3;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void isFebruary29thValid() {
        final int day = 29;
        final int year = 1988;
        final int month = 1;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = true;
        assertEquals(output, expected);
    }

    @Test
    public void isFebruary30thValid() {
        final int day = 30;
        final int year = 1988;
        final int month = 1;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void isJune31stValid() {
        final int day = 31;
        final int year = 1988;
        final int month = 5;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void isNegativeDayValid() {
        final int day = -1;
        final int year = 1988;
        final int month = 1;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void isNovember31stValid() {
        final int day = 31;
        final int year = 1988;
        final int month = 10;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void isSeptember31stValid() {
        final int day = 31;
        final int year = 1988;
        final int month = 8;
        final boolean output = TimeUtil.isDateValid(year, month, day);
        final boolean expected = false;
        assertEquals(output, expected);
    }

    @Test
    public void testToLocalWithTimezoneLondon() {
        final DateTimeZone defaultZone = DateTimeZone.getDefault();
        assertTrue(
                "The London timezone should be known. These are the known timezones: "
                        + getKnownTimezones(), DateTimeZone.getAvailableIDs()
                        .contains(londonTimezoneTag));
        DateTimeZone.setDefault(DateTimeZone.forID(londonTimezoneTag));
        final long timeStamp = 1386045542000L;
        final String expectedOutput = "2013-12-03 04:39:02+00:00";
        final String output = TimeUtil.toLocal(timeStamp);
        Assert.assertEquals(expectedOutput, output);
        DateTimeZone.setDefault(defaultZone);
    }

    @Test
    public void testToLocalWithTimezoneNepal() {
        final DateTimeZone defaultZone = DateTimeZone.getDefault();
        Assert.assertTrue(
                "The Kathmandu timezone should be known. These are the known timezones: "
                        + getKnownTimezones(), DateTimeZone.getAvailableIDs()
                        .contains(kathmanduTimezoneTag));
        DateTimeZone.setDefault(DateTimeZone.forID(kathmanduTimezoneTag));
        final long timeStamp = 1386045542000L;
        final String expectedOutput = "2013-12-03 10:24:02+05:45";
        final String output = TimeUtil.toLocal(timeStamp);
        Assert.assertEquals(expectedOutput, output);
        DateTimeZone.setDefault(defaultZone);
    }

    @Test
    public void testToLocalWithTimezoneSaoPaulo() {
        final DateTimeZone defaultZone = DateTimeZone.getDefault();
        Assert.assertTrue(
                "The Sao Paulo timezone should be known. These are the known timezones: "
                        + getKnownTimezones(), DateTimeZone.getAvailableIDs()
                        .contains(saoPauloTimezoneTag));
        DateTimeZone.setDefault(DateTimeZone.forID(saoPauloTimezoneTag));
        final long timeStamp = 1386045542000L;
        final String expectedOutput = "2013-12-03 02:39:02-02:00";
        final String output = TimeUtil.toLocal(timeStamp);
        Assert.assertEquals(expectedOutput, output);
        DateTimeZone.setDefault(defaultZone);
    }

    @Test
    public void testToLocalWithTimezoneVienna() {
        final DateTimeZone defaultZone = DateTimeZone.getDefault();
        Assert.assertTrue(
                "The Vienna timezone should be known. These are the known timezones: "
                        + getKnownTimezones(), DateTimeZone.getAvailableIDs()
                        .contains(viennaTimezoneTag));
        DateTimeZone.setDefault(DateTimeZone.forID(viennaTimezoneTag));
        final long timeStamp = 1386045542000L;
        final String expectedOutput = "2013-12-03 05:39:02+01:00";
        final String output = TimeUtil.toLocal(timeStamp);
        Assert.assertEquals(expectedOutput, output);
        DateTimeZone.setDefault(defaultZone);
    }

    @Test
    public void testToUtc() {
        final long timeStamp = 1386045542000L;
        final String expectedOutput = "2013-12-03 04:39:02+00:00";
        final String output = TimeUtil.toUtc(timeStamp);
        Assert.assertEquals(expectedOutput, output);
    }

    /**
     * Gets all the known timezones.
     *
     */
    private String getKnownTimezones() {
        return DateTimeZone.getAvailableIDs().toString();
    }

}
