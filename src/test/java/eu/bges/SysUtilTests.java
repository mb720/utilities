package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.google.common.base.Optional;

import eu.bges.SysUtil.OS;

public class SysUtilTests {

    private static final String TESTFILE_DIR = "/testfiles/";
    private static final String DIR_SCRIPT = TESTFILE_DIR + "dirScript.bat";
    private static final String BAT_ECHO_SCRIPT = TESTFILE_DIR
            + "echoScript.bat";
    private static final String LS_SCRIPT = TESTFILE_DIR + "lsScript.sh";
    private static final String SHELL_ECHO_SCRIPT = TESTFILE_DIR
            + "echoScript.sh";
    private static final String UMLAUT_STRING = "String with umlauts: ÄÖÜäöü";
    /**
     * cmd.exe issues this warning when an unknown command is entered
     */
    private static final String COMMAND_IS_NOT_RECOGNIZED = "is not recognized as an internal";

    @Test
    public void callBatScript() {

        // Call batch scripts only on Windows
        assumeTrue(SysUtil.getOS().equals(OS.WINDOWS));
        final File script = IOUtil.getResource(DIR_SCRIPT, getClass());
        final String pathToScript = script.getAbsolutePath();
        final Optional<String> output = SysUtil.call(pathToScript);
        if (output.isPresent()) {
            assertFalse("Calling " + DIR_SCRIPT
                    + " should produce non-empty output", output.get()
                    .isEmpty());
        } else {
            fail("Calling " + DIR_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callBatScriptWithArrayParams() {

        // Call batch scripts only on Windows
        assumeTrue(SysUtil.getOS().equals(OS.WINDOWS));
        final File script = IOUtil.getResource(BAT_ECHO_SCRIPT, getClass());
        final String pathToScript = script.getAbsolutePath();
        final String[] command = { pathToScript, "Hello", "World" };
        final Optional<String> output = SysUtil.call(command);
        if (output.isPresent()) {
            // 'echo' prints a system dependent newline at the end
            final String expectedOutput = "Hello World\r\n";
            final String actualOutput = output.get();
            assertEquals(BAT_ECHO_SCRIPT + " should print " + expectedOutput
                    + " to the console", expectedOutput, actualOutput);
        } else {
            fail("Calling " + BAT_ECHO_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callBatScriptWithListParams() {

        // Call batch scripts only on Windows
        assumeTrue(SysUtil.getOS().equals(OS.WINDOWS));
        final File script = IOUtil.getResource(BAT_ECHO_SCRIPT, getClass());
        final String pathToScript = script.getAbsolutePath();
        final List<String> command = Arrays.asList(pathToScript, "Hello",
                "World");
        final Optional<String> output = SysUtil.call(command);
        if (output.isPresent()) {
            // 'echo' prints a system dependent newline at the end
            final String expectedOutput = "Hello World\r\n";
            final String actualOutput = output.get();
            assertEquals(BAT_ECHO_SCRIPT + " should print " + expectedOutput
                    + " to the console", expectedOutput, actualOutput);
        } else {
            fail("Calling " + BAT_ECHO_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callShellScript() {

        // Call shell scripts only on Linux
        assumeTrue(SysUtil.getOS().equals(OS.LINUX));
        final File script = IOUtil.getResource(LS_SCRIPT, getClass());
        /*
         * Jenkins (or whoever runs the tests) needs to be able to execute the
         * script
         */
        script.setExecutable(true);
        final String pathToScript = script.getAbsolutePath();
        final Optional<String> output = SysUtil.call(pathToScript);
        if (output.isPresent()) {
            assertFalse("Calling " + LS_SCRIPT
                    + " should produce non-empty output", output.get()
                    .isEmpty());
        } else {
            fail("Calling " + LS_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callShellScriptWithArrayParams() {

        // Call shell scripts only on Linux
        assumeTrue(SysUtil.getOS().equals(OS.LINUX));
        final File script = IOUtil.getResource(SHELL_ECHO_SCRIPT, getClass());
        /*
         * Jenkins (or whoever runs the tests) needs to be able to execute the
         * script
         */
        script.setExecutable(true);
        final String pathToScript = script.getAbsolutePath();
        final String[] command = { pathToScript, "Hello", "World" };
        final Optional<String> output = SysUtil.call(command);
        if (output.isPresent()) {
            // 'echo' prints a system dependent newline at the end
            final String expectedOutput = "Hello World\n";
            final String actualOutput = output.get();
            assertEquals(SHELL_ECHO_SCRIPT + " should print " + expectedOutput
                    + " to the console", expectedOutput, actualOutput);
        } else {
            fail("Calling " + SHELL_ECHO_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callShellScriptWithListParams() {

        // Call shell scripts only on Linux
        assumeTrue(SysUtil.getOS().equals(OS.LINUX));
        final File script = IOUtil.getResource(SHELL_ECHO_SCRIPT, getClass());
        /*
         * Jenkins (or whoever runs the tests) needs to be able to execute the
         * script
         */
        script.setExecutable(true);
        final String pathToScript = script.getAbsolutePath();
        final List<String> command = Arrays.asList(pathToScript, "Hello",
                "World");
        final Optional<String> output = SysUtil.call(command);
        if (output.isPresent()) {
            // 'echo' prints a system dependent newline at the end
            final String expectedOutput = "Hello World\n";
            final String actualOutput = output.get();
            assertEquals(SHELL_ECHO_SCRIPT + " should print " + expectedOutput
                    + " to the console", expectedOutput, actualOutput);
        } else {
            fail("Calling " + SHELL_ECHO_SCRIPT + " should succeed");
        }
    }

    @Test
    public void callWithNoCommand() {
        final Optional<String> output = SysUtil.call();
        assertFalse("The call should have no output", output.isPresent());
    }

    @Test
    public void clipboardTest() {

        SysUtil.copyToClipboard(UMLAUT_STRING);
        final String clipContent = SysUtil.getClipboardString();
        assertTrue("The content of the clipboard should be: " + UMLAUT_STRING,
                clipContent.equals(UMLAUT_STRING));
    }

    @Test
    public void cmdWithInvalidCommand() {
        /*
         * This tests makes only sense when executed on a windows machine ->
         * 'assumeTrue' only executes the test if it's run on Windows
         */
        assumeTrue(SysUtil.getOS().equals(OS.WINDOWS));
        final Optional<String> output = SysUtil.cmd("dirz", "/r");
        assertTrue("The call should return a string as its output",
                output.isPresent());
        assertTrue("The command should not be recognized", output.get()
                .contains(COMMAND_IS_NOT_RECOGNIZED));
    }

    @Test
    public void cmdWithValidCommand() {
        // This tests makes only sense when executed on a windows machine
        assumeTrue(SysUtil.getOS().equals(OS.WINDOWS));
        final Optional<String> output = SysUtil.cmd("dir", "/r");
        assertTrue("The call should return a string as its output",
                output.isPresent());
        assertFalse("The ouput should not be empty", output.get().isEmpty());
    }

    @Test
    public void getClassPathFiles() {
        final List<File> files = SysUtil.getClasspathFiles();
        assertFalse("There should be files in the classpath", files.isEmpty());
    }

}
