package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.common.base.Optional;

public class XmlUtilTests {
    private static final Logger LOG = LoggerFactory
            .getLogger(XmlUtilTests.class);

    private final File RIP_LANG_TEST_XML = IOUtil.getResource(
            "/testfiles/xml/RIPLang_1031.xml", getClass());
    private final File RIPSERVER_WITH_ERROR_XML = IOUtil.getResource(
            "/testfiles/xml/ripserver_with_error.xml", getClass());
    private final File NON_EXISTENT_FILE = IOUtil.getResource(
            "file_in_nirvana.xml", getClass());
    private final File RIPSERVER_XML = IOUtil.getResource(
            "/testfiles/xml/ripserver.xml", getClass());
    private static final String AUTHOR_TAG = "author";

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private static final String BOOK_TAG = "book";
    private final File booksXml = IOUtil.getResource(
            "/testfiles/xml/books.xml", getClass());
    private final File corruptFile = IOUtil.getResource(
            "/testfiles/corrupt.html", getClass());

    /**
     * Files are tested whether they conform to this XML scheme
     */
    private final static String RIPPARAMETERFILE_XML_SCHEME = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;\n&lt;schema xmlns=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:tns=&quot;http://www.mges.at/RIP/RIPParameterFile/&quot;\n	targetNamespace=&quot;http://www.mges.at/RIP/RIPParameterFile/&quot;&gt;\n	&lt;element name=&quot;RIPLang&quot; type=&quot;tns:RIPParameterFileType&quot;&gt;&lt;/element&gt;\n\n	&lt;complexType name=&quot;EntryType&quot;&gt;\n		&lt;sequence&gt;\n			&lt;element name=&quot;Key&quot; type=&quot;string&quot; maxOccurs=&quot;1&quot; minOccurs=&quot;1&quot;&gt;&lt;/element&gt;\n			&lt;element name=&quot;Value&quot; type=&quot;string&quot; maxOccurs=&quot;1&quot; minOccurs=&quot;1&quot;&gt;&lt;/element&gt;\n		&lt;/sequence&gt;\n	&lt;/complexType&gt;\n\n	&lt;complexType name=&quot;RIPParameterFileType&quot;&gt;\n		&lt;sequence&gt;		\n			&lt;element name=&quot;Entry&quot; type=&quot;tns:EntryType&quot; maxOccurs=&quot;unbounded&quot; minOccurs=&quot;1&quot;&gt;&lt;/element&gt;\n		&lt;/sequence&gt;\n	&lt;/complexType&gt;\n\n&lt;/schema&gt;\n";

    @Test
    public void allBooksInBooksXmlShouldHaveContent() {
        final List<Node> books = XmlUtil.getNodes(booksXml, BOOK_TAG);
        for (final Node book : books) {
            final String bookContent = book.getTextContent().trim();
            assertFalse("All of the books should have content",
                    bookContent.isEmpty());
        }
    }

    @Test
    public void booksXmlShouldHaveTwelveBooks() {
        final List<Node> books = XmlUtil.getNodes(booksXml, BOOK_TAG);
        final int expectedNrOfBooks = 12;
        assertEquals(expectedNrOfBooks, books.size());
    }

    @Test
    public void createTag() {
        final String myTag = "myTag";
        final String myVal = "myVal";
        final String expected = "<" + myTag + ">" + myVal + "</" + myTag + ">";
        final String actual = XmlUtil.createElement(myTag, myVal);
        assertEquals(expected, actual);
    }

    @Test
    public void createTagWithEmptyVal() {
        final String myTag = "myTag";
        final String myVal = "";
        final String expected = "<" + myTag + ">" + myVal + "</" + myTag + ">";
        final String actual = XmlUtil.createElement(myTag, myVal);
        assertEquals(expected, actual);
    }

    @Test
    public void createTagWithNull() {
        final String myTag = "myTag";
        final String expected = "<" + myTag + ">null</" + myTag + ">";
        final String actual = XmlUtil.createElement(myTag, null);
        assertEquals(expected, actual);
    }

    @Test
    public void gettingNonExistingKeyReturnsEmptyListOfNodes() {
        final List<Node> books = XmlUtil.getNodes(booksXml, "");
        assertTrue(books.isEmpty());
    }

    @Test
    public void gettingNullKeyReturnsEmptyListOfNodes() {
        final List<Node> books = XmlUtil.getNodes(booksXml, null);
        assertTrue(books.isEmpty());
    }

    @Test
    public void parseInvalidXmlFileReturnsEmptyOptional() {
        final Optional<Document> doc = XmlUtil.parseXmlFile(corruptFile);
        assertFalse(doc.isPresent());
    }

    @Test
    public void parseValidXmlFileHasOneChild() {
        final Optional<Document> doc = XmlUtil.parseXmlFile(booksXml);
        if (doc.isPresent()) {
            final Node firstChild = doc.get().getFirstChild();
            assertFalse(booksXml + "should have at least one child",
                    firstChild == null);
        } else {
            fail("Should be able to parse " + booksXml);
        }
    }

    @Test
    public void parsingCorruptFileReturnsEmptyListOfNodes() {
        final List<Node> books = XmlUtil.getNodes(corruptFile, BOOK_TAG);
        assertTrue(books.isEmpty());
    }

    @Test
    public void parsingFromNullStreamReturnsThrowsIllegalArgumentException() {
        final InputStream stream = null;
        exception.expect(IllegalArgumentException.class);
        XmlUtil.getNodes(stream, BOOK_TAG);
    }

    @Test
    public void parsingNonExistingFileReturnsEmptyListOfNodes() {
        final List<Node> books = XmlUtil.getNodes(new File(""), BOOK_TAG);
        assertTrue(books.isEmpty());
    }

    @Test
    public void validateXmlTests() {
        // xml file valid, but wrong scheme
        assertFalse("XML file has different scheme", XmlUtil.validateXml(
                RIP_LANG_TEST_XML, RIPPARAMETERFILE_XML_SCHEME));
        // xml file invalid
        assertFalse("Invalid XML should be invalid against correct scheme",
                XmlUtil.validateXml(RIPSERVER_WITH_ERROR_XML,
                        RIPPARAMETERFILE_XML_SCHEME));
        // non-existing xml file
        assertFalse("Non-existing file should be invalid", XmlUtil.validateXml(
                NON_EXISTENT_FILE, RIPPARAMETERFILE_XML_SCHEME));
        // xml file valid, correct scheme
        assertTrue("Valid XML should be valid against correct scheme",
                XmlUtil.validateXml(RIPSERVER_XML, RIPPARAMETERFILE_XML_SCHEME));
        // xml file valid, invalid scheme
        assertFalse("XML should be invalid against invalid scheme",
                XmlUtil.validateXml(RIPSERVER_XML, "blabla"));
        // xml file valid, scheme null
        assertFalse("XML should be invalid against null scheme",
                XmlUtil.validateXml(RIPSERVER_XML, null));
        assertFalse("Null file should be invalid",
                XmlUtil.validateXml(null, null));
    }

    @Test
    public void xmlSubTestElementTestWithAuthor() {

        final Optional<Document> docMaybe = XmlUtil.parseXmlFile(booksXml);
        if (docMaybe.isPresent()) {
            final Document doc = docMaybe.get();
            final Element element = doc.getDocumentElement();
            final String[] subTextElem = XmlUtil.getXmlSubTextElement(element,
                    AUTHOR_TAG);
            LOG.info(Arrays.asList(subTextElem).toString());
        } else {
            fail("Should be able to parse " + booksXml);
        }
    }
}
