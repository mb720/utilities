package eu.bges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.File;

import org.junit.Test;

import com.google.common.base.Optional;

import eu.bges.IOUtil;
import eu.bges.JavaScriptUtil;

public class JavaScriptUtilTests {
	private static final String NEW_VALUE = "some new value";
	private static final String TEST_KEY = "checkThisProject";
	private static final String JSON_PATH = "/testfiles/json.json";
	private static final String EXPECTED_VAL = "C:/Users/marakai/juno_workspace/CheckThisCode/";

	@Test
	public void createJavaScriptCall() {
		final String functionName = "alert";
		final String param = "Hello World";
		final String call = JavaScriptUtil.createCall(functionName, param);
		assertFalse("The created call should not be empty", call.isEmpty());
	}

	/**
	 * Set a variable in a JSON file representing a map. Then confirm that the
	 * value is correctly set by reading it from the file. Then reset the
	 * variable to its old value.
	 */
	@Test
	public void getAndSetJson() {

		final File jsonFile = IOUtil.getResource(JSON_PATH, getClass());
		final Optional<String> prevValMaybe = JavaScriptUtil.setValInJson(
				TEST_KEY, NEW_VALUE, jsonFile);
		if (prevValMaybe.isPresent()) {
			final String prevVal = prevValMaybe.get();
			assertEquals("The original value should be " + EXPECTED_VAL,
					prevVal, EXPECTED_VAL);
			final Optional<String> newValMaybe = JavaScriptUtil.getValFromJson(
					TEST_KEY, jsonFile);
			if (newValMaybe.isPresent()) {
				final String newVal = newValMaybe.get();
				assertEquals(
						"The newly set value read from the JSON file is different from the one that was set",
						newVal, NEW_VALUE);
				// Reset the file to the old value
				JavaScriptUtil.setValInJson(TEST_KEY, EXPECTED_VAL, jsonFile);
			} else {
				fail("The newly set value could not be read from file "
						+ jsonFile);
			}

		} else {
			fail("Expected the key " + TEST_KEY + " to exist in "
					+ jsonFile.getAbsolutePath());
		}
		JavaScriptUtil.getValFromJson(TEST_KEY, jsonFile);
	}

	@Test
	public void getValFromJson() {

		final String jsonFilePath = IOUtilTests.class.getResource(JSON_PATH)
				.getPath();
		final File jsonFile = new File(jsonFilePath);
		final Optional<String> valMaybe = JavaScriptUtil.getValFromJson(
				TEST_KEY, jsonFile);
		if (valMaybe.isPresent()) {
			assertEquals(valMaybe.get(), EXPECTED_VAL);

		} else {
			fail("Should have been able to parse the key " + TEST_KEY
					+ " from " + jsonFile.getAbsolutePath());
		}
	}
}
