package eu.bges;

import java.io.File;
import java.util.Locale;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.bges.LocaleUtil.LangTag;

public class GraphicsUtilTests {
    private final File smileyPng = IOUtil.getResource(
            "/testfiles/pics/smiley.PNG", getClass());
    private final File smileyGif = IOUtil.getResource(
            "/testfiles/pics/smiley.GIF", getClass());
    private final File smileyJpg = IOUtil.getResource(
            "/testfiles/pics/smiley.JPG", getClass());

    @Test
    public void gifFileIsSupported() {

        assertTrue("GIF should be supported",
                GraphicsUtil.isGraphicFile(smileyGif));
    }

    @Test
    public void gifFileIsSupportedInTurkey() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.TURKISH);

        final boolean isSupported = GraphicsUtil.isGraphicFile(smileyGif);
        // Reset locale
        Locale.setDefault(oldLocale);
        assertTrue("GIF should be supported in Turkey", isSupported);
    }

    @Test
    public void jpgFileIsSupported() {
        assertTrue("JPG should be supported",
                GraphicsUtil.isGraphicFile(smileyJpg));
    }

    @Test
    public void jpgFileIsSupportedInTurkey() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.TURKISH);
        final boolean isSupported = GraphicsUtil.isGraphicFile(smileyJpg);
        // Reset locale
        Locale.setDefault(oldLocale);
        assertTrue("JPG should be supported in Turkey", isSupported);
    }

    @Test
    public void pngFileIsSupported() {
        assertTrue("PNG should be supported",
                GraphicsUtil.isGraphicFile(smileyPng));
    }

    @Test
    public void pngFileIsSupportedInTurkey() {
        final Locale oldLocale = Locale.getDefault();
        LocaleUtil.setLocaleTo(LangTag.TURKISH);
        final boolean isSupported = GraphicsUtil.isGraphicFile(smileyPng);
        // Reset locale
        Locale.setDefault(oldLocale);
        assertTrue("PNG should be supported in Turkey", isSupported);
    }
}
