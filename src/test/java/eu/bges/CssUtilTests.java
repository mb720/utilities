package eu.bges;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;

import org.junit.Test;

import com.google.common.base.Optional;

import eu.bges.CssUtil;
import eu.bges.IOUtil;

public class CssUtilTests {

	private static final String CORRUPT_CSS = "/testfiles/corrupt.css";
	private static final String INVALID_CSS = "/testfiles/invalid.css";
	private static final String GRAPH_STYLE_CSS = "/testfiles/graph_style.css";
	private static final String EXPECTED_BODY_WIDTH = "919px";

	@Test
	public void getBodyWidth() {
		final File css = IOUtil.getResource(GRAPH_STYLE_CSS, getClass());
		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final Optional<String> valMaybe = CssUtil.getValue(css, selector, prop);
		if (valMaybe.isPresent()) {
			final String val = valMaybe.get();
			assertTrue("Expected val: " + EXPECTED_BODY_WIDTH + "; got " + val,
					val.equals(EXPECTED_BODY_WIDTH));
		} else {
			fail("Could not find property " + prop + " in selector " + selector
					+ " in CSS file " + css.getAbsolutePath());
		}

	}

	@Test
	public void getBodyWidthFromCorruptFile() {
		final File css = IOUtil.getResource(CORRUPT_CSS, getClass());
		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final Optional<String> valMaybe = CssUtil.getValue(css, selector, prop);
		// There should be no value found in the corrupted file
		assertFalse(valMaybe.isPresent());
	}

	@Test
	public void getBodyWidthFromInvalidCss() {
		final File css = IOUtil.getResource(INVALID_CSS, getClass());
		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final Optional<String> valMaybe = CssUtil.getValue(css, selector, prop);
		if (valMaybe.isPresent()) {
			final String val = valMaybe.get();
			assertTrue("Expected val: " + EXPECTED_BODY_WIDTH + "; got " + val,
					val.equals(EXPECTED_BODY_WIDTH));
		} else {
			fail("Could not find property " + prop + " in selector " + selector
					+ " in CSS file " + css.getAbsolutePath());
		}

	}

	@Test
	public void setBodyWidth() {
		final File css = IOUtil.getResource(GRAPH_STYLE_CSS, getClass());

		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final boolean success = CssUtil.setValue(css, selector, prop,
				EXPECTED_BODY_WIDTH);
		assertTrue(success);
	}

	@Test
	public void setBodyWidthInCorruptFile() {
		final File css = IOUtil.getResource(CORRUPT_CSS, getClass());
		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final boolean success = CssUtil.setValue(css, selector, prop,
				EXPECTED_BODY_WIDTH);
		assertFalse(success);
	}

	/**
	 * Although a CSS rule is malformed it should still be possible to set the
	 * value of a property within a valid rule.
	 */
	@Test
	public void setBodyWidthInInvalidCss() {
		final File css = IOUtil.getResource(INVALID_CSS, getClass());
		final String selector = CssUtil.BODY;
		final String prop = CssUtil.WIDTH;
		final boolean success = CssUtil.setValue(css, selector, prop,
				EXPECTED_BODY_WIDTH);
		assertTrue(success);

	}
}
