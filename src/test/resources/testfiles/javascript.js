//// Parameters used for the state machine diagram ////
var diagramWidth = 880;
var diagramHeight = 800;
var diagramIsEditable = true;
var showTransitionNames = true;
var stateFontFamily = 'sans-serif';
var defaultStateColor = '#F5FA5C';
var activeStateColor = '#13F256';
// Highlight this transition as the current one
var currentTransitionId = '';

//// Settings pertaining to the XML that describes the state chart ////
var xmlState = 'STATE';
var xmlTransition = 'TRANSITION';
