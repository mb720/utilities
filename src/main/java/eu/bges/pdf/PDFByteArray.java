/*
 * Copyright 2014 Martin Gangl
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges.pdf;

import eu.bges.data.ByteArrayContainer;

/**
 * A container class which contains a single byte array representing a PDF file.
 *
 * @author Martin Gangl
 */
public class PDFByteArray extends ByteArrayContainer {

    // Serialization support
    private static final long serialVersionUID = 1L;

    // Vars
    private final byte[] byteArray;

    /**
     * @param byteArray
     *            byte array containing the actual PDF data
     * @param description
     *            for this containers element
     */
    public PDFByteArray(final byte[] byteArray, final String description) {
        this.byteArray = byteArray;
        setDescription(description);
    }

    @Override
    public byte[] getByteArray() {
        return byteArray;
    }

    @Override
    public boolean isValid() {
        return byteArray != null && byteArray.length > 0
                && getDescription().length() >= MIN_DESCRIPTION_LENGTH;
    }
}