/*
 * Copyright 2014 Matthias Braun, Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Optional;

/**
 * Contains methods used for dealing with XML content. This includes parsing XML
 * files or strings.
 *
 * @author Matthias Braun
 * @author Martin Gangl
 *
 */
public final class XmlUtil {

    /**
     * The 'less than' sign < escaped for HTML
     */
    private static final String ESCAPED_LESS_THAN = "&lt;";

    private static final String XML_SCHEMA_2001 = "http://www.w3.org/2001/XMLSchema";

    private static final Logger LOG = LoggerFactory.getLogger(XmlUtil.class);

    private static final String NO_NULL_PARAMS = "Only non-null input is allowed";

    /**
     * Parses files, strings, and {@link InputStream}s containing XML.
     */
    private static DocumentBuilder builder;

    /** This is a utility class not meant to be instantiated by others. */
    private XmlUtil() {
    }

    /**
     * Converts a {@link NodeList} into a {@link List} of {@link Node}s.
     * <p>
     * This is useful for traversing nodes using {@code foreach}:
     * {@link NodeList} doesn't implement the {@link Iterable} interface.
     *
     * @param nodes
     *            the {@link NodeList} to convert
     * @return the {@code nodes} as a {@link List} of {@link Node}s
     */
    public static List<Node> asList(final NodeList nodes) {

        final List<Node> nodeList = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {

            final Node node = nodes.item(i);
            nodeList.add(node);
        }
        return nodeList;
    }

    /**
     * Creates an XML element like {@code <myTag>1234</myTag>}
     * <p>
     * If the {@code name} or the {@code value} are null, they are represented
     * as "null" in the returned string.
     *
     * @param name
     *            the name of the element. This string is used to create the
     *            opening and closing tags around the value
     *
     * @param value
     *            the value that goes between the tags
     * @return the value surrounded by an opening and a closing tag
     */
    public static String createElement(final String name, final Object value) {
        return "<" + name + ">" + value + "</" + name + ">";
    }

    /**
     * Gets the nodes of a certain {@code tag} from an XML file.
     * <p>
     * Note that the XML in the file must have a single root node.
     *
     * @param xmlFile
     *            the file containing XML code
     * @param tag
     *            the nodes of this tag are parsed from {@code xmlFile}
     * @return a {@link List} of {@link Node}s of the type {@code tag}
     */
    public static List<Node> getNodes(final File xmlFile, final String tag) {

        if (!xmlFile.exists()) {
            LOG.warn("File does not exist: {}", xmlFile);
        }
        return getNodes(IOUtil.read(xmlFile), tag);
    }

    /**
     * Gets the nodes of a certain {@code tag} from an {@link InputStream}
     * containing XML code.
     * <p>
     * Note that the XML in the stream must have a single root node.
     * <p>
     * Returns an empty list if {@code tag} is {@code null}.
     *
     * @param inputStream
     *            the {@link InputStream} containing XML code
     * @param tag
     *            the nodes of this tag are parsed from {@code inputStream}.
     *            Must not be null
     * @return a {@link List} of {@link Node}s of the type {@code tag}
     */
    public static List<Node> getNodes(final InputStream inputStream,
            final String tag) {
        List<Node> nodeList = new ArrayList<>();
        if (tag == null) {
            LOG.warn("Can't get null tag from XML");
        } else {
            final DocumentBuilder builder = getDocBuilder();
            try {
                final Document doc = builder.parse(inputStream);
                doc.getDocumentElement().normalize();
                final NodeList nodes = doc.getElementsByTagName(tag);
                nodeList = asList(nodes);
            } catch (SAXException | IOException e) {
                LOG.warn("Could not parse nodes for tag {}", tag, e);
            }
        }
        return nodeList;
    }

    /**
     * Gets the nodes of a certain {@code tag} from an XML string.
     * <p>
     * UTF-8 is used to parse {@code xmlSstr}. Note that {@code xmlSstr} must
     * have a single root node.
     *
     * @param xmlStr
     *            the string containing XML code
     * @param tag
     *            the nodes of this tag are parsed from {@code xmlStr}
     * @return a {@link List} of {@link Node}s of the type {@code tag}
     */
    public static List<Node> getNodes(final String xmlStr, final String tag) {
        final InputStream is = new ByteArrayInputStream(
                xmlStr.getBytes(IOUtil.UTF_8));
        return getNodes(is, tag);
    }

    /**
     * @param element
     *            which should be loaded
     * @param tagNames
     *            array of tag names which's values should be retrieved
     * @return a string array containing the entry values in the same order as
     *         the corresponding tagNames
     */
    public static String[] getXmlSubTextElement(final Element element,
            final String... tagNames) {
        String[] result = null;
        if (tagNames != null && tagNames.length > 0) {
            result = new String[tagNames.length];
            for (int i = 0; i < result.length; i++) {
                result[i] = getXmlTextValue(element, tagNames[i]);
            }
        }
        return result;
    }

    /**
     * @param element
     *            of which the value should be retrieved
     * @param tagName
     *            name of the tag which's value should be retrieved
     * @return the value or null if invalid parameters were given
     */
    public static String getXmlTextValue(final Element element,
            final String tagName) {
        String textVal = null;
        final NodeList nl = element.getElementsByTagName(tagName);

        for (final Node n : asList(nl)) {
            LOG.info(n.toString());
        }

        if (nl != null && nl.getLength() > 0) {
            final Element el = (Element) nl.item(0);
            if (el != null && el.getFirstChild() != null) {
                textVal = el.getFirstChild().getNodeValue();
            }
        }
        return textVal;
    }

    /**
     * Parses an XML {@code file}.
     *
     * @param file
     *            XML file to be parsed
     * @return the {@link Document} object wrapped in an {@link Optional} if an
     *         error occurred (e.g. file not found)
     */
    public static Optional<Document> parseXmlFile(final File file) {

        Objects.requireNonNull(file, NO_NULL_PARAMS);
        Document parsedDoc = null;
        if (file.exists()) {
            try {
                parsedDoc = getDocBuilder().parse(file);
            } catch (SAXException | IOException e) {
                LOG.warn("Could not parse {}", file, e);
            }
        } else {
            LOG.warn("File {} does not exist", file);
        }
        return Optional.fromNullable(parsedDoc);
    }

    /**
     * Tests if a {@code xmlFile} conforms to a {@code schema}.
     *
     * @param xmlFile
     *            to be validated
     * @param schema
     *            the {@code xmlFile} is tested against. Is unescaped if needed
     * @return true if and only if the XML file is valid against the given
     *         scheme
     */
    public static boolean validateXml(final File xmlFile, String schema) {

        boolean xmlIsValid = true;
        if (xmlFile == null || schema == null) {
            xmlIsValid = false;
        } else {
            if (schema.startsWith(ESCAPED_LESS_THAN)) {
                schema = unescapeXml(schema);
            }
            final DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            factory.setNamespaceAware(true);
            final SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XML_SCHEMA_2001);

            final Source[] schemas = new Source[] { new StreamSource(
                    new StringReader(schema)) };
            try {
                factory.setSchema(schemaFactory.newSchema(schemas));
                final DocumentBuilder docBuilder = factory.newDocumentBuilder();
                final SimpleXmlErrorHandler handler = new SimpleXmlErrorHandler();
                docBuilder.setErrorHandler(handler);
                docBuilder.parse(xmlFile);
                // The XML is valid if no error occurred while parsing
                xmlIsValid = !handler.errorOccurred;
            } catch (SAXException | IOException | ParserConfigurationException e) {
                xmlIsValid = false;
                LOG.info(e.getMessage(), e);
            }
        }
        return xmlIsValid;
    }

    /**
     * Lazily creates a {@link DocumentBuilder} that can parse XML.
     *
     * @return a {@link DocumentBuilder}
     */
    private static DocumentBuilder getDocBuilder() {
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (final ParserConfigurationException e) {
            LOG.warn("Could not get XML document builder", e);
        }
        return builder;
    }

    /**
     * Converts a string whose special characters (<, >, &, etc) have been
     * escaped ({@code &lt; &gt; &amp;}) back to its original form.
     *
     * @param unescapeThis
     *            the string whose characters should be unescaped
     * @return the unescaped string
     */
    private static String unescapeXml(final String unescapeThis) {
        return StringEscapeUtils.unescapeXml(unescapeThis);
    }

    /**
     * A simple {@link ErrorHandler} for XML files.
     *
     * @author Matthias Braun
     * @author Martin Gangl
     *
     */
    private static class SimpleXmlErrorHandler implements ErrorHandler {

        private boolean errorOccurred = false;

        @Override
        public void error(final SAXParseException exception)
                throws SAXException {

            errorOccurred = true;
            LOG.warn(exception.getMessage(), exception);
        }

        @Override
        public void fatalError(final SAXParseException exception)
                throws SAXException {

            errorOccurred = true;
            LOG.warn(exception.getMessage(), exception);
        }

        @Override
        public void warning(final SAXParseException exception)
                throws SAXException {

            errorOccurred = true;
            LOG.warn(exception.getMessage(), exception);
        }

    }

}
