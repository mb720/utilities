/*
 * Copyright 2014 Matthias Braun
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges;

import com.google.common.base.Optional;

/**
 * Provides methods to inspect Java programs at runtime.
 *
 * @author Matthias Braun
 *
 */
public final class ReflectionUtil {

    /** This is a utility class not meant to be instantiated by others. */
    private ReflectionUtil() {
    }

    /**
     * Gets the first stack frame of a caller by its class name.
     *
     * @param callerName
     *            fully qualified class name of a caller
     * @return stackframe associated with the caller
     */
    public static Optional<StackTraceElement> getCallerStackFrame(
            final String callerName) {
        StackTraceElement callerFrame = null;

        final StackTraceElement[] stack = new Throwable().getStackTrace();
        // Search the stack trace to find the calling class
        for (final StackTraceElement frame : stack) {
            if (frame.getClassName().equals(callerName)) {
                callerFrame = frame;
                /*
                 * If we look any further we might find a stackframe from
                 * another method of the same class
                 */
                break;
            }
        }
        return Optional.fromNullable(callerFrame);
    }

    /**
     * Gets a {@code Thread} by its ID.
     *
     * @param threadId
     *            the ID of the thread
     * @return the {@code Thread} wrapped in an {@code Optional} in case it
     *         wasn't found
     */
    public static Optional<Thread> getThread(final long threadId) {
        Thread thread = null;
        for (final Thread t : Thread.getAllStackTraces().keySet()) {
            if (t.getId() == threadId) {
                thread = t;
                break;
            }
        }
        return Optional.fromNullable(thread);
    }
}
