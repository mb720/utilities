/*
 * Copyright 2014 Matthias Braun, Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;

import eu.bges.strings.StringUtil;

/**
 * Provides utility functions for accessing the operating system and the Java
 * class path.
 *
 * @author Matthias Braun
 * @author Martin Gangl
 *
 */
public final class SysUtil {

    /**
     * The key to the operating system's name in
     * {@link System#getProperty(String)}.
     */
    public static final String OS_NAME = "os.name";

    private static final Logger LOG = LoggerFactory.getLogger(SysUtil.class);
    /**
     * Prefix these strings if you want to execute a command like 'dir' on
     * cmd.exe under Windows.
     */
    private static final String CMD_COMMAND = "cmd", SLASH_C = "/c";
    /**
     * This string should be in every value returned by
     * {@code System.getProperty("os.name)"} when executed on any Windows
     * machine.
     */
    private static final String WINDOWS_OS_NAME = "Windows";
    /**
     * This string should be in every value returned by
     * {@code System.getProperty("os.name)"} when executed on any Linux machine.
     */
    private static final String LINUX_OS_NAME = "Linux";
    /**
     * This string should be in every value returned by
     * {@code System.getProperty("os.name)"} when executed on any Mac machine.
     */
    private static final String MAC_OS_NAME = "MacOS";

    /** This is a utility class not meant to be instantiated by others. */
    private SysUtil() {
    }

    /**
     * Adds all files from a {@code directory} to the classpath.
     *
     * @param directory
     *            whose contained files should be added to the classpath
     * @return whether all files from the {@code directory} were added
     *         successfully. False if one of them was already on the classpath
     */
    public static boolean addDirToClasspath(final File directory) {
        boolean success = false;
        if (directory != null && directory.exists()) {
            success = true;
            for (final File file : directory.listFiles()) {
                final URI uri = file.toURI();
                try {
                    success = success && addURLToClasspath(uri.toURL());
                } catch (final MalformedURLException e) {
                    LOG.warn(e.getMessage(), e);
                }
            }
        }
        return success;
    }

    /**
     * Adds a file, identified by a {@code url}, to the classpath.
     *
     * @param url
     *            the {@link URL} to be added to the classpath
     * @return whether adding the {@code url} was successful. False, if it was
     *         already on the classpath
     */
    public static boolean addURLToClasspath(final URL url) {
        boolean success = true;
        try (final DynClassLoader dynLoader = new DynClassLoader(
                (URLClassLoader) ClassLoader.getSystemClassLoader())) {

            // Is the URL already on the classpath?
            final boolean alreadyonCP = StringUtil.containsIgnoreCase(
                    url.toString(), StringUtil.toStrings(dynLoader.getURLs()));

            if (alreadyonCP) {
                success = false;
            } else {
                dynLoader.addURL(url);
            }
        } catch (final IOException e) {
            success = false;
            LOG.warn("Could not add {} to classpath", url, e);
        }
        return success;
    }

    /**
     * Calls an executable and returns its output.
     * <p>
     * The executable will execute in its own thread.
     *
     * @param command
     *            path to the executable (can be a script, a .bat file, etc.)
     *            and its parameters
     * @param dir
     *            directory from which the command is executed
     * @return output of the executable as a string wrapped in an
     *         {@link Optional} in case the executable could not be called or
     *         its output could not be read
     */
    public static Optional<String> call(@Nullable final File dir,
            final String... command) {

        String output = null;
        final ProcessBuilder pb = new ProcessBuilder(command);
        if (pb.command().isEmpty()) {
            LOG.warn("No command given");
        } else {
            /*
             * Set the directory from which the command is executed. If dir is
             * null, the current directory of the Java thread is used.
             */
            pb.directory(dir);
            // Don't fail silently --> This lets us receive error messages
            pb.redirectErrorStream(true);

            final String charset = Charsets.UTF_8.displayName();
            // Start the process and read its output
            try (final InputStreamReader reader = new InputStreamReader(pb
                    .start().getInputStream(), charset)) {
                output = CharStreams.toString(reader);
            } catch (final IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return Optional.fromNullable(output);
    }

    /**
     * Calls a command and returns its output.
     * <p>
     * The command will execute in its own thread.
     *
     * @param command
     *            the command to be executed as a {@link List}. This can be the
     *            path to an executable and its parameters
     * @return the output of the executable as a string wrapped in an
     *         {@link Optional} in case the executable could not be called or
     *         its output could not be read
     */
    public static Optional<String> call(final List<String> command) {
        // Convert the list to an array
        final String[] cmdAsArray = command.toArray(new String[command.size()]);
        return call(cmdAsArray);
    }

    /**
     * Calls a command and returns its output.
     * <p>
     * The command will execute in its own thread.
     *
     * @param command
     *            the path to the executable (can be a script, a .bat file,
     *            etc.) and its parameters
     * @return the output of the executable as a string wrapped in an
     *         {@link Optional} in case the executable could not be called or
     *         its output could not be read
     */
    public static Optional<String> call(final String... command) {
        return call(null, command);
    }

    /**
     * Windows only: Calls a {@code command} via cmd.exe.
     * <p>
     * {@code cmd /c} is prepended to the {@code command} to make it executable
     * via the cmd window shell.
     *
     * @param command
     *            the command to call
     * @return the output of the executable as a string wrapped in an
     *         {@link Optional} in case the executable could not be called or
     *         its output could not be read failed
     */
    public static Optional<String> cmd(final String... command) {

        final List<String> combinedCommand = new ArrayList<String>();
        combinedCommand.addAll(getWindowsCmdPrefix());
        combinedCommand.addAll(Lists.newArrayList(command));
        return call(combinedCommand);
    }

    /**
     * Copies a string to the system clipboard.
     *
     * @param str
     *            the string that is copied to the clipboard
     */
    public static void copyToClipboard(final String str) {
        final StringSelection data = new StringSelection(str);

        final Clipboard clipboard = Toolkit.getDefaultToolkit()
                .getSystemClipboard();
        clipboard.setContents(data, data);
    }

    /**
     * Gets a list of all files and directories currently on the classpath.
     * <p>
     * This is useful for debugging a {@link NoSuchMethodException} which occurs
     * when a method is invoked that was added in a later version of a library
     * than is currently loaded. Both versions of the library are on the
     * classpath but the classloader loads the older version. The solution is to
     * remove the older version.
     *
     * @return list of all files that are on the class path
     */
    public static List<File> getClasspathFiles() {

        final List<File> files = new ArrayList<>();
        final ClassLoader loader = ClassLoader.getSystemClassLoader();

        if (loader instanceof URLClassLoader) {
            final URL[] urls = ((URLClassLoader) loader).getURLs();
            for (final URL url : urls) {
                files.add(new File(url.getFile()));
            }
        } else {
            LOG.warn("Could not get class path files because the loader is not and instance of URLClassLoader");
        }

        return files;
    }

    /**
     * Gets the string that is currently saved in the system clipboard.
     *
     * @return current clipboard string or the empty string if the contents of
     *         the clipboard could not be accessed
     */
    public static String getClipboardString() {
        String clipStr = "";
        final Clipboard clipboard = Toolkit.getDefaultToolkit()
                .getSystemClipboard();
        Transferable clipboardData = null;
        try {
            // The parameter is not used according to the documentation
            clipboardData = clipboard.getContents(null);
        } catch (final IllegalStateException e) {
            LOG.warn("System clipboard is currently not available", e);
        }
        if (clipboardData != null) {
            if (clipboardData.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    clipStr = String.valueOf(clipboardData
                            .getTransferData(DataFlavor.stringFlavor));
                } catch (UnsupportedFlavorException | IOException e) {
                    LOG.warn("Could not get contents of system clipboard", e);
                }
            } else {
                LOG.warn("The system clipboard does not support strings as a data flavor");
            }
        }
        return clipStr;
    }

    /**
     * Gets the current operating system this JVM is running on.
     *
     * @return the current operating system
     * @see <a href="http://www.osgi.org/Specifications/Reference#os">OS
     *      names</a>
     */
    public static OS getOS() {
        final OS currOs;
        final String currOsName = System.getProperty(OS_NAME);

        if (StringUtil.containsIgnoreCase(currOsName, WINDOWS_OS_NAME)) {
            currOs = OS.WINDOWS;
        } else if (StringUtil.containsIgnoreCase(currOsName, MAC_OS_NAME)) {
            currOs = OS.MAC;
        } else if (StringUtil.containsIgnoreCase(currOsName, LINUX_OS_NAME)) {
            currOs = OS.LINUX;
        } else {
            currOs = OS.OTHER;
        }
        return currOs;
    }

    /**
     * Gets the prefix that is needed if a call to cmd.exe is made.
     * <p>
     * It's {@code cmd /c}.
     *
     * @return the prefix for calling cmd.exe as a list
     */
    private static List<String> getWindowsCmdPrefix() {

        final List<String> cmdPrefix = new ArrayList<String>();
        cmdPrefix.add(CMD_COMMAND);
        cmdPrefix.add(SLASH_C);
        return cmdPrefix;
    }

    /**
     * Different operating systems.
     *
     * @author Matthias Braun
     *
     */
    public enum OS {
        MAC, WINDOWS, LINUX, OTHER
    }

    /**
     * A {@link URLClassLoader} that exposes the
     * {@link DynClassLoader#addURL(URL)} method to dynamically add files to the
     * classpath.
     *
     * @author Matthias Braun
     *
     */
    private static class DynClassLoader extends URLClassLoader {
        /**
         * Creates a new instance of this class.
         *
         * @param classLoader
         *            the new instance will contain the URLs of this
         *            {@code classLoader}
         */
        public DynClassLoader(final URLClassLoader classLoader) {
            super(classLoader.getURLs());
        }

        @Override
        public void addURL(final URL url) {
            super.addURL(url);
        }
    }
}
