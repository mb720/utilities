package eu.bges.crypto;

import java.util.zip.Checksum;

/**
 * TODO: Please document these methods
 *
 * @author Martin Gangl
 *
 */
public class CRC16 implements Checksum {

	/** value contains the currently computed CRC, set it to 0 initially */
	private int value = 0;

	@Override
	public long getValue() {
		return value;
	}

	/** reset CRC value to 0 */
	@Override
	public void reset() {
		value = 0;
	}

	/**
	 * Updates the CRC with {@code aByte}
	 *
	 * @param aByte
	 *            the byte with which the CRC is updated
	 */
	public void update(final byte aByte) {
		int a, b;

		a = aByte;
		for (int count = 7; count >= 0; count--) {
			a = a << 1;
			b = (a >>> 8) & 1;
			if ((value & 0x8000) != 0) {
				value = ((value << 1) + b) ^ 0x1021;
			} else {
				value = (value << 1) + b;
			}
		}
		value = value & 0xffff;
		return;
	}

	/**
	 * @param b
	 *            the array of bytes to update the checksum with
	 */
	public void update(final byte[] b) {
		if (b != null) {
			update(b, 0, b.length);
		}
	}

	@Override
	public void update(final byte[] b, final int off, final int len) {
		if (b != null && off >= 0 && off < b.length && off + len <= b.length) {
			for (int i = off; i < off + len; i++) {
				update(b[i]);
			}
		}
	}

	@Override
	public void update(final int b) {
		update((byte) b);

	}
}