package eu.bges.crypto;

/**
 * Represents a Universally Unique Identifier of a computer.
 * <p>
 * A UUID is considered strong if it was generated from the computer's hardware
 * characteristics (e.g., serial number of mainboard, serial number of graphics
 * card). It's considered weak if it was generated only from the computer's
 * architecture and/or OS version.
 *
 * @author Martin Gangl
 *
 */
public class ComputerUUID {

    // Vars
    private final boolean isStrong;
    private final String uuid;

    /**
     * @param isStrong
     *            true if a strong (hardware dependent) uuid is present
     * @param uuid
     *            to be used (must not be null)
     * @throws IllegalArgumentException
     *             if illegal (null) uuid was given
     */
    public ComputerUUID(final boolean isStrong, final String uuid)
            throws IllegalArgumentException {
        if (uuid == null) {
            throw new IllegalArgumentException("UUID must not be null!");
        }
        this.isStrong = isStrong;
        this.uuid = uuid;
    }

    @Override
    public boolean equals(final Object other) {
        if (other instanceof ComputerUUID) {
            return ((ComputerUUID) other).getUuid().equals(uuid);
        }
        return false;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    /**
     * @return the isStrong
     */
    public boolean isStrong() {
        return isStrong;
    }

    @Override
    public String toString() {
        return this.uuid;
    }
}