/**
 * Contains utilities related to cryptography, random numbers, and authentication.
 * @author Matthias Braun
 *
 */
package eu.bges.crypto;