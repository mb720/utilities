/*
 * Copyright 2014 Matthias Braun
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges.log;

import java.io.PrintStream;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import com.google.common.base.Optional;

import eu.bges.ReflectionUtil;
import eu.bges.TimeUtil;

/**
 * Provides methods and constants for {@link Handler}s that write log messages
 * to the console.
 *
 * @author Matthias Braun
 *
 */
public abstract class AbstractConsoleHandler extends Handler {

    /**
     * Separate the log message and the string that tells us when and where a
     * message was logged (meta info) with some spaces.
     */
    private static final int FIXED_OFFSET = 10;

    /**
     * The name a thread gets if its real one couldn't be found.
     */
    public static final String UNKNOWN_THREAD_NAME = "unknown";

    @Override
    public void close() {
        // No resource to close
    }

    @Override
    public void flush() {
        // Nothing to flush
    }

    @Override
    public abstract void publish(LogRecord record);

    /**
     * Creates a link that's clickable in the Eclipse console to the line where
     * the log message was created.
     *
     *
     * @param callerFrame
     *            the {@link StackTraceElement} of the class that issued the log
     *            message
     * @return the link to the line where the log message was created
     */
    protected String getLinkToLine(final StackTraceElement callerFrame) {
        final String fileName = callerFrame.getFileName();
        final int lineNumber = callerFrame.getLineNumber();
        // This creates a link to the line when used in Eclipse
        return '(' + fileName + ':' + lineNumber + ')';
    }

    /**
     * Gets the location of the logged message. This includes file name, line
     * number, and the fully qualified method name.
     * <p>
     * Example: {@code (MyClass.java:48) my.package.MyClass.myMethod}
     * <p>
     * The filename with the line number inside the parentheses work as a link
     * in the Eclipse console.
     *
     * @param record
     *            the {@link LogRecord} containing the logged message and
     *            information about it
     * @return the location of the log message in a format that works as link in
     *         the Eclipse console
     */
    protected String getMsgLocation(final LogRecord record) {

        final StringBuilder location = new StringBuilder();
        final String loggingClass = record.getSourceClassName();
        final Optional<StackTraceElement> callerFrameMaybe = ReflectionUtil
                .getCallerStackFrame(loggingClass);
        if (callerFrameMaybe.isPresent()) {

            final String linkToLine = getLinkToLine(callerFrameMaybe.get());
            location.append(linkToLine + ' ');
        }
        // Add the fully qualified method that issued the message
        location.append(loggingClass).append('.')
        .append(record.getSourceMethodName());
        return location.toString();
    }

    /**
     * Prints a log message to the console. If the message {@link LogRecord}
     * contains also a {@link Throwable}, it is printed as well.
     *
     * @param msg
     *            the log message to print.
     * @param msgInfo
     *            information like where and when the message was logged
     * @param record
     *            the {@link LogRecord} containing the log {@code msg}
     */
    protected void printMsg(final String msg, final String msgInfo,
            final LogRecord record) {
        final PrintStream stream;
        if (record.getLevel() == Level.SEVERE) {
            // Error messages are printed in red
            stream = System.err;
        } else {
            // All INFO, DEBUG and WARN messages are sent to System.out
            stream = System.out;
        }

        /*
         * Print first the message and then where (and when) it was logged
         */
        final String msgFormat = twoStringsFormat(msg.length() + FIXED_OFFSET);
        stream.printf(msgFormat, msg, msgInfo);

        if (record.getThrown() != null) {
            // The record contains a throwable
            printThrowable(record, stream);
        }

    }

    /**
     * Prints the information about a throwable like its stack trace.
     * <p>
     * The output should look like Java's standard stack trace format.
     *
     * @param record
     *            the {@link LogRecord} containing the throwable that was thrown
     * @param stream
     *            the {@link PrintStream} used to print the information. For
     *            example {@code System.out} or {@code System.err}
     */
    protected void printThrowable(final LogRecord record,
            final PrintStream stream) {

        final StringBuilder msg = new StringBuilder();
        final Throwable thrown = record.getThrown();
        final int threadId = record.getThreadID();

        final Optional<Thread> threadMaybe = ReflectionUtil.getThread(threadId);
        final String threadName = threadMaybe.isPresent() ? threadMaybe.get()
                .getName() : UNKNOWN_THREAD_NAME;

                // The local time with its offset to UTC
                final String time = TimeUtil.toLocal(record.getMillis());
                msg.append(time).append(": Exception in thread \"").append(threadName)
                .append("\" ");
                msg.append(thrown.getClass().getName()).append(": ");

                final String throwableMsg = thrown.getMessage();
                if (throwableMsg != null) {
                    msg.append(throwableMsg);
                }
                for (final StackTraceElement elem : thrown.getStackTrace()) {
                    final String fileName = elem.getFileName();
                    final String className = elem.getClassName();
                    final String methodName = elem.getMethodName();
                    final int lineNumber = elem.getLineNumber();
                    msg.append("\n\tat ").append(className).append('.')
                    .append(methodName).append('(').append(fileName)
                    .append(':').append(lineNumber).append(')');
                }
                stream.println(msg);
    }

    /**
     * Creates a format that separates two strings by some spaces and puts a
     * system-independent new line at the end.
     * <p>
     * This format can be used by {@link PrintStream#printf(String, Object...)}.
     *
     * @param offset
     *            number of spaces that separates the two strings
     * @return a format that separates two strings
     */
    protected String twoStringsFormat(final int offset) {
        /*
         * The percentage sign creates an offset, the dash left-justifies the
         * first string
         */
        return "%-" + offset + "s%s%n";
    }
}
