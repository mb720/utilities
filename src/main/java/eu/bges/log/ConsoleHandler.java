/*
 * Copyright 2014 Matthias Braun
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges.log;

import java.util.logging.LogRecord;

import org.eclipse.jdt.annotation.Nullable;

import eu.bges.TimeUtil;

/**
 * Handles log messages by printing them to the console. Includes the location
 * and the time the message was logged.
 *
 * <p>
 * Example log line:
 * <p>
 * {@code INFO: My log message (MyClass.java:48) my.package.MyClass.myMethod 2013-12-24 15:29:16+00:00}
 * <p>
 * To receive log messages this class must be listed in the logging.properties
 * file of its project.
 *
 * @author Matthias Braun
 */
public class ConsoleHandler extends AbstractConsoleHandler {

    /**
     * The published log entry should look like this:
     * {@code <Log level><Log message><Some whitespace><Link to originating line><Fully qualified name of method><Date and time with UTC offset>}
     * Example log line:
     * {@code INFO: My log message (MyClass.java:48) my.package.MyClass.myMethod 2013-12-24 15:29:16+00:00}
     */
    @Override
    public void publish(@Nullable final LogRecord record) {

        if (record == null) {
            System.err.println(getClass().getName()
                    + ": Can not print log record because it is null");
        } else {
            final String logLevelAndMsg = record.getLevel() + ": "
                    + record.getMessage();

            // The local time with its offset to UTC
            final String time = TimeUtil.toLocal(record.getMillis());

            final String msgLocationAndTime = getMsgLocation(record) + ' '
                    + time;

            printMsg(logLevelAndMsg, msgLocationAndTime, record);
        }
    }
}
