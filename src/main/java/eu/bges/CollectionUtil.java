/*
 * Copyright 2014  Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges;

import java.util.Arrays;

/**
 * Utilities for dealing with collections.
 *
 * @author Martin Gangl
 */
public class CollectionUtil {
    /**
     * Checks whether an {@code array} contains a {@code value}.
     *
     * @param array
     *            object array to be searched
     * @param value
     *            to be searched within the array
     * @return true if and only if the given array and value are not null and if
     *         the array contains an object which is equal to the given value
     *         object (based on the object.equals() method)
     */
    public static boolean arrayContains(final Object[] array, final Object value) {
        return Arrays.asList(array).contains(value);
    }

    /**
     * Merges multiple arrays of {@code int}s.
     *
     * @param arrays
     *            merge these arrays
     * @return the merged array of the arrays given
     */
    public static int[] arrayMerge(final int[]... arrays) {
        // Determine required size of new array
        int count = 0;
        for (final int[] array : arrays) {
            if (array != null) {
                count += array.length;
            }
        }
        final int[] mergedArray = new int[count];

        // Merge each array into new array
        int start = 0;
        for (final int[] array : arrays) {
            if (array != null) {
                System.arraycopy(array, 0, mergedArray, start, array.length);
                start += array.length;
            }
        }
        return mergedArray;
    }

    /**
     * Merges multiple arrays of {@code long}s.
     *
     * @param arrays
     *            merge these arrays
     * @return the merged array of the arrays given
     */
    public static long[] arrayMerge(final long[]... arrays) {
        // Determine required size of new array
        int count = 0;
        for (final long[] array : arrays) {
            if (array != null) {
                count += array.length;
            }
        }
        final long[] mergedArray = new long[count];

        // Merge each of the arrays into mergedArray
        int start = 0;
        for (final long[] array : arrays) {
            if (array != null) {
                System.arraycopy(array, 0, mergedArray, start, array.length);
                start += array.length;
            }
        }
        return mergedArray;
    }

}
