/*
 * Copyright 2014 Matthias Braun
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges;

import java.io.File;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.regex.Pattern;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.bges.strings.StringUtil;

/**
 * Constants and utility methods for handling JavaScript files.
 *
 * @author Matthias Braun
 */
public final class JavaScriptUtil {

    /**
     * Matches JavaScript variables and remembers the name as well as the value.
     * <p>
     * Name and value are accessible using a {@link java.util.regex.Matcher
     * Matcher}. The following examples should match: "var myVar = myVal;",
     * "var   varAfterSomeSpaces=myValDirectlyAfterEquals", "  var myVar =myVal"
     * <p>
     * Note: ? after a quantifier makes it a reluctant quantifier, it tries to
     * find the smallest match.
     */
    public static final Pattern JS_PATTERN = Pattern.compile(
            "\\s*var\\s+(.+?)\\s*=\\s*(.+?);?", Pattern.CASE_INSENSITIVE);

    /**
     * Used to convert Java objects to JSON format.
     */
    private static Gson gson = new Gson();

    /** This is a utility class not meant to be instantiated by others. */
    private JavaScriptUtil() {
    }

    /**
     * Create a JavaScript function call.
     *
     * @param functionName
     *            The name of the function to call.
     * @param params
     *            Parameters of the function.
     * @return A string that can be passed to browser and executed as a
     *         JavaScript function.
     */
    public static String createCall(final String functionName,
            final Object... params) {
        final StringBuilder jsonParams = new StringBuilder();
        // Convert each parameter into a JSON string
        for (final Object p : params) {
            jsonParams.append(toJson(p)).append(',');
        }
        // Remove the trailing comma
        StringUtil.replaceLast(jsonParams, ",", "");

        // Return the function with its parameters
        return functionName + "(" + jsonParams + ");";
    }

    /**
     * Convert a JSON file into a map from string to string.
     *
     * @param file
     *            The JSON file to parse.
     * @return The contents of the file as a map from string to object.
     */
    public static Map<String, String> fromJson(final File file) {
        final Type mapType = new TypeToken<Map<String, String>>() {
        }.getType();
        final String jsonStr = IOUtil.read(file);
        return fromJson(jsonStr, mapType);
    }

    /**
     * Deserialize a JSON string to an object of {@code T}.
     *
     * @param jsonStr
     *            The string of JSON to deserialize.
     * @param type
     *            The type of the object that will be the result of the
     *            deserialization.
     * @param <T>
     *            This is the same as {@code type} and represents the type of
     *            the deserialized object.
     * @return The deserialized object.
     */
    public static <T> T fromJson(final String jsonStr, final Type type) {
        // Return an object of the specified type
        return gson.fromJson(jsonStr, type);
    }

    /**
     * Get the value of a map inside a JSON file.
     *
     * @param key
     *            The key to the value.
     * @param file
     *            The JSON file containing the map.
     * @return The value from the map wrapped in a
     *         {@link com.google.common.base.Optional Optional} in case the
     *         value wasn't found.
     */
    public static Optional<String> getValFromJson(final String key,
            final File file) {
        final Map<String, String> map = fromJson(file);

        return Optional.fromNullable(map.get(key));
    }

    /**
     * Update a variable within a JSON file.
     *
     * @param varName
     *            The name of the variable within the JSON file that's updated.
     * @param newVal
     *            The value the variable will have after the update.
     * @param file
     *            The JSON file that's updated.
     * @return The value the variable had before the update or an empty
     *         {@link Optional} if it didn't exist.
     */
    public static Optional<String> setValInJson(final String varName,
            final String newVal, final File file) {
        final Map<String, String> map = fromJson(file);

        final String prevValue = map.put(varName, newVal);
        final String updatedJson = toJson(map);
        IOUtil.write(updatedJson, file);
        return Optional.fromNullable(prevValue);

    }

    /**
     * Convert a Java object into a JSON string.
     *
     * @param obj
     *            The Java object.
     * @return The JSON string.
     */
    public static String toJson(final Object obj) {
        return gson.toJson(obj);
    }
}
