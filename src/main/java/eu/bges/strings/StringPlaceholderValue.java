/*
 * Copyright 2014 Martin Gangl
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges.strings;

/**
 * Container class used for transferring the actual data content for the
 * placeholders to the placeholder processor in
 * {@link StringUtil#processStringPlaceholders(String, java.util.List)}.
 *
 * @author Martin Gangl
 */
public class StringPlaceholderValue {

    private final String definition;
    private final String value;

    /**
     * @param definition
     *            of the stringplaceholder. This will be replaced by
     *            {@code value}
     * @param value
     *            for the stringplaceholder. Will replace {@code definition}
     */
    public StringPlaceholderValue(final String definition, final String value) {
        if (definition != null) {
            this.definition = definition;
        } else {
            this.definition = "";
        }
        if (value != null) {
            this.value = value;
        } else {
            this.value = "";
        }
    }

    /**
     * The definition of the placeholder will be replaced by the placeholder's
     * value.
     *
     * @return the definition
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * The value of the placeholder will replace the placeholder's definition.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }
}