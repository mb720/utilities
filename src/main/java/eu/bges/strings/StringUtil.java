/*
 * Copyright 2014 Matthias Braun, Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.bges.MathUtil;

/**
 * Methods for string searching, comparison, and manipulation.
 *
 * @author Matthias Braun
 * @author Martin Gangl
 *
 */
public final class StringUtil {
	private static final Logger LOG = LoggerFactory.getLogger(StringUtil.class);

	private static final String NO_NULL_PARAMS = "Only non-null input is allowed";

	/**
	 * This number is returned by {@link String#indexOf(String)} when it didn't
	 * find a character or a string.
	 */
	public static final int POS_NOT_FOUND = -1;

	/**
	 * The 52 letters of the English alphabet from A to z: AaBbCcDd...
	 */
	public static final List<Character> UPPER_LOWER_ALPHABET = Arrays.asList(
			'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G',
			'g', 'H', 'h', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm',
			'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T',
			't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z');

	/** This is a utility class not meant to be instantiated by others. */
	private StringUtil() {
	}

	/**
	 * Determines which of two characters comes first in the English alphabet.
	 * <p>
	 * If they are the same letter (for example {@code I} and {@code i}), the
	 * uppercase letter is considered to come first.
	 * <p>
	 * If the input character is not one of the 52 upper- or lowercase letters,
	 * the char's Unicode position is used to determine its position. This
	 * ensures that letters come first.
	 * <p>
	 * The result is locale-independent.
	 *
	 * @return an int value that is negative if {@code first} comes before
	 *         {@code second} in the alphabet, zero if the two are equal, and
	 *         positive if {@code first} comes after {@code second} in the
	 *         alphabet.
	 */
	public static int alphabeticalCompare(final char first, final char second) {
		int firstPos = UPPER_LOWER_ALPHABET.indexOf(first);
		if (firstPos == POS_NOT_FOUND) {
			firstPos = UPPER_LOWER_ALPHABET.size() + first;
		}
		int secondPos = UPPER_LOWER_ALPHABET.indexOf(second);
		if (secondPos == POS_NOT_FOUND) {
			secondPos = UPPER_LOWER_ALPHABET.size() + second;
		}
		return firstPos - secondPos;
	}

	/**
	 * Determines whether a {@code list} contains a {@code string},
	 * case-insensitive.
	 *
	 * @param string
	 *            that might be in the {@code list}
	 * @param list
	 *            that might contain the {@code string}
	 * @return whether {@code string} is in the {@code list}, ignoring case
	 * @throws NullPointerException
	 *             if any input parameter is null
	 */
	public static boolean containsIgnoreCase(final String string,
			final List<String> list) {

		Objects.requireNonNull(string, NO_NULL_PARAMS);
		Objects.requireNonNull(list, NO_NULL_PARAMS);
		boolean isContained = false;

		for (final String strFromList : list) {
			if (string.equalsIgnoreCase(strFromList)) {
				isContained = true;
				break;
			}
		}
		return isContained;
	}

	/**
	 * Determines whether one string contains another one, ignoring their cases.
	 * <p>
	 * Both of the strings must not be null. Both the {@code src} and the
	 * {@code searchStr} are converted to lower case. Use English as the locale
	 * when calling {@code toLowerCase} to avoid bugs if the default locale is
	 * Turkish.
	 *
	 * @param src
	 *            source string that might contain the search string
	 * @param searchStr
	 *            string searched in the source string
	 * @return whether the source string contains the searched string,
	 *         regardless of case
	 * @see <a href="http://java.sys-con.com/node/46241">Turkish Java Needs
	 *      Special Brewing</a>
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static boolean containsIgnoreCase(final String src,
			final String searchStr) {
		Objects.requireNonNull(src, NO_NULL_PARAMS);
		Objects.requireNonNull(searchStr, NO_NULL_PARAMS);
		// Avoid Turkish locale bug
		return src.toLowerCase(Locale.ENGLISH).contains(
				searchStr.toLowerCase(Locale.ENGLISH));
	}

	/**
	 * Counts the occurrences of one string in another.
	 *
	 * @param countThis
	 *            string to count
	 * @param insideThat
	 *            search the string inside this text
	 * @return the number of times {@code countThis} occurs in
	 *         {@code insideThat}
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static int count(final String countThis, final String insideThat) {
		Objects.requireNonNull(countThis, NO_NULL_PARAMS);
		Objects.requireNonNull(insideThat, NO_NULL_PARAMS);

		return getOccurrences(countThis, insideThat).size();
	}

	/**
	 * Deletes all characters between two occurrences of {@code delim}.
	 * <p>
	 * If {@code delim} is empty, return the unaltered {@code input}.
	 * <p>
	 * Example: deleteBetween("Remove my 'unnecessary' quotes 'now' please","'")
	 * returns "Remove my &nbsp;&nbsp;quotes&nbsp;&nbsp;please".
	 *
	 * @param src
	 *            input string that will have its content deleted between
	 *            {@code delim}
	 * @param delim
	 *            delimiter: All strings between two of these strings are
	 *            removed from {@code src}.
	 * @return {@code input} without the parts between {@code delim}.
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String deleteBetween(final String src, final String delim) {

		Objects.requireNonNull(src, NO_NULL_PARAMS);
		Objects.requireNonNull(delim, NO_NULL_PARAMS);
		final String output;
		if (delim.isEmpty()) {
			output = src;
		} else {
			final int delimCount = count(src, delim);
			if (delimCount % 2 == 0) {
				final StringBuilder goodParts = new StringBuilder();
				final String[] parts = src.split(delim);
				/*
				 * The odd parts contain the substrings between the delimiters
				 * -> Dismiss the odd parts and add the even parts
				 */
				for (int i = 0; i < parts.length; i += 2) {
					goodParts.append(parts[i]);
				}
				output = goodParts.toString();
			} else {
				LOG.warn(
						"Can't remove between {} because it occurs {} times in the input string which is an odd number.",
						delim, delimCount);
				output = src;
			}
		}

		return output;
	}

	/**
	 * Deletes a char from a string at the specified position.
	 *
	 * @param input
	 *            string who will be one character shorter afterwards if
	 *            {@code pos} is within bounds
	 * @param pos
	 *            character at this position is removed
	 * @return the string without the character at {@code pos}
	 * @throws NullPointerException
	 *             if {@code input} is null
	 * @throws IndexOutOfBoundsException
	 *             if {@code pos} is out of {@code input}'s bounds
	 *
	 */
	public static String deleteCharAt(final String input, final int pos) {
		Objects.requireNonNull(input, NO_NULL_PARAMS);
		if (pos < 0 || pos >= input.length()) {
			throw new IndexOutOfBoundsException();
		}

		final StringBuilder buf = new StringBuilder(input);
		buf.deleteCharAt(pos);
		return buf.toString();

	}

	/**
	 * Deletes a range of characters from a string.
	 * <p>
	 * For example
	 * {@code deleteRange("my small example",3,8) returns "my example"}.
	 *
	 * @param src
	 *            original string that has a range of its characters removed
	 * @param from
	 *            position of the first deleted character
	 * @param to
	 *            position of the last deleted character
	 * @return the string without the character range {@code from} to {@code to}
	 *         inclusively
	 *
	 * @throws NullPointerException
	 *             if {@code src} string is null
	 * @throws IndexOutOfBoundsException
	 *             if {@code from} or {@code to} are out of bounds
	 */
	public static String deleteRange(final String src, final int from,
			final int to) {
		Objects.requireNonNull(src, NO_NULL_PARAMS);
		if (from < 0 || from >= src.length() || to < 0 || to >= src.length()) {
			throw new IndexOutOfBoundsException();
		}

		final String firstHalf = src.substring(0, from);
		// The last character of the range is deleted as well, therefore +1
		final String secondHalf = src.substring(to + 1);
		return firstHalf + secondHalf;
	}

	/**
	 * Calculates the Levenshtein distance between {@code str1} and {@code str2}
	 * .
	 *
	 * @param str1
	 *            first string whose similarity to {@code str2} is calculated
	 * @param str2
	 *            second string whose similarity to {@code str1} is calculated
	 * @return the similarity in terms of the Levenshtein distance between the
	 *         two strings
	 * @throws NullPointerException
	 *             if any input string is null
	 * @see <a
	 *      href="https://en.wikipedia.org/wiki/Levenshtein_distance#Iterative_with_full_matrix">Levenshtein
	 *      distance</a>
	 */
	public static int getDistance(final String str1, final String str2) {
		Objects.requireNonNull(str1, NO_NULL_PARAMS);
		Objects.requireNonNull(str2, NO_NULL_PARAMS);
		final int lenStr1 = str1.length();
		final int lenStr2 = str2.length();

		/**
		 * For all i and j, dist[i,j] will hold the Levenshtein distance between
		 * the first i characters of str1 and the first j characters of str2;
		 * note that dist has (m+1)*(n+1) values
		 */
		final int[][] dist = new int[lenStr1 + 1][lenStr2 + 1];

		// Initialize the distance array
		for (int i = 0; i < lenStr1; i++) {
			dist[i][0] = i;
		}
		for (int j = 0; j < lenStr2; j++) {
			dist[0][j] = j;
		}

		for (int i = 1; i <= str1.length(); i++) {
			for (int j = 1; j <= str2.length(); j++) {
				// One if a substitution of chars is necessary, otherwise zero
				final int substitutionCost = (str1.charAt(i - 1) == str2
						.charAt(j - 1)) ? 0 : 1;
				dist[i][j] = MathUtil.min(dist[i - 1][j] + 1, // deletion
						dist[i][j - 1] + 1, // insertion
						dist[i - 1][j - 1] + substitutionCost); // substitution

			}
		}
		return dist[str1.length()][str2.length()];
	}

	/**
	 * Searches a string inside another one and returns the positions where
	 * occurrences of the searched string start.
	 * <p>
	 * If {@code searchThis} is empty, an empty list is returned.
	 * <p>
	 * Example: {@code getOccurrences("*","*|||*|*")} returns 0, 4, and 6.
	 *
	 * @param searchThis
	 *            the start positions of this string are searched
	 * @param insideThat
	 *            we search inside that string
	 * @return the start positions of {@code searchThis} within
	 *         {@code insideThat}
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static List<Integer> getOccurrences(final String searchThis,
			final String insideThat) {
		Objects.requireNonNull(searchThis, NO_NULL_PARAMS);
		Objects.requireNonNull(insideThat, NO_NULL_PARAMS);
		final List<Integer> occurrences = new ArrayList<>();
		int start = 0;
		if (!searchThis.isEmpty()) {
			while (true) {
				final int occurrence = insideThat.indexOf(searchThis, start);
				if (occurrence == POS_NOT_FOUND) {
					break;
				} else {
					occurrences.add(occurrence);
					start = occurrence + searchThis.length();
				}
			}
		}
		return occurrences;
	}

	/**
	 * Replaces the {@code placeholders} with their values within a {@code text}
	 * .
	 *
	 * @param text
	 *            the string that has its placeholders replaced
	 * @param placeholders
	 *            these placeholders are replaced with their real value
	 * @return the modified text string or null if {@code text} is null
	 */
	public static String processStringPlaceholders(String text,
			final List<StringPlaceholderValue> placeholders) {
		if (text != null) {
			for (final StringPlaceholderValue spv : placeholders) {
				text = text.replace(spv.getDefinition(), spv.getValue());
			}
		}
		return text;
	}

	/**
	 * Removes the last character of a string.
	 *
	 * @param string
	 *            to be shortened by one character
	 * @return {@code string} without its last character; null will be returned
	 *         for a null string, "" for an empty string
	 */
	public static String removeLastChar(final String string) {
		if (string == null || string.length() == 0) {
			return string;
		}
		return string.substring(0, string.length() - 1);
	}

	/**
	 * Replaces every occurrence of a string within a text with another string.
	 * Case is ignored.
	 * <p>
	 * Example: replaceIgnoreCase("oRigINal text", "Original" "Changed") returns
	 * "Changed text".
	 *
	 * @param original
	 *            original string that contains the string to replace
	 * @param replaceThis
	 *            replace this string
	 * @param withThat
	 *            with that string
	 * @return text with all occurrences of the searched string replaced or the
	 *         original string if the original string does not contain the
	 *         string to be replaced
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String replaceIgnoreCase(final String original,
			final String replaceThis, final String withThat) {
		Objects.requireNonNull(original, NO_NULL_PARAMS);
		Objects.requireNonNull(replaceThis, NO_NULL_PARAMS);
		Objects.requireNonNull(withThat, NO_NULL_PARAMS);

		String result;
		if (containsIgnoreCase(original, replaceThis)) {
			final Pattern p = Pattern.compile(replaceThis,
					Pattern.CASE_INSENSITIVE);
			final Matcher m = p.matcher(original);
			result = m.replaceAll(withThat);
		} else {
			result = original;
		}
		return result;

	}

	/**
	 * Replaces the last occurrence of a substring within a string with another
	 * string.
	 *
	 * @param original
	 *            the original string that contains the string to replace
	 * @param replaceThis
	 *            the string whose last occurrence is replaced {@code withThat}
	 * @param withThat
	 *            replaces {@code replaceThis}
	 * @return a copy of the original string with the last occurrence of the
	 *         substring replaced. Or the unaltered source if the source didn't
	 *         contain the substring
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String replaceLast(final String original,
			final String replaceThis, final String withThat) {
		Objects.requireNonNull(original, NO_NULL_PARAMS);
		Objects.requireNonNull(replaceThis, NO_NULL_PARAMS);
		Objects.requireNonNull(withThat, NO_NULL_PARAMS);
		final StringBuilder builder = new StringBuilder(original);
		replaceLast(builder, replaceThis, withThat);

		return builder.toString();
	}

	/**
	 * Replaces the last occurrence of a string within a {@link StringBuilder}
	 * with another string.
	 *
	 * @param original
	 *            the {@link StringBuilder} whose content is changed
	 * @param replaceThis
	 *            replace the last occurrence of this string within the builder
	 *            {@code withThat}
	 * @param withThat
	 *            the string replacing the last occurrence of
	 *            {@code replaceThis}
	 * @throws NullPointerException
	 *             if any of the arguments are null
	 */
	public static void replaceLast(final StringBuilder original,
			final String replaceThis, final String withThat) {
		Objects.requireNonNull(original, NO_NULL_PARAMS);
		Objects.requireNonNull(replaceThis, NO_NULL_PARAMS);
		Objects.requireNonNull(withThat, NO_NULL_PARAMS);

		final int index = original.lastIndexOf(replaceThis);
		if (index != -1) {
			original.replace(index, index + replaceThis.length(), withThat);
		}
	}

	/**
	 * @param s
	 *            input string, should contain an integer value somewhere
	 * @return the first integer value found in the input string; 0 will be
	 *         returned if an input string with no digits has been given
	 */
	public static int searchAndParseFirstDigitFromString(final String s) {
		try {
			final Pattern pat = Pattern.compile("\\d+");
			final Matcher match = pat.matcher(s);
			match.find();
			return Integer.parseInt(match.group());
		} catch (final Exception ex) {
			return 0;
		}
	}

	/**
	 * Gets every character of {@code orig} after the first occurrence of string
	 * {@code start}.
	 * <p>
	 * For example when {@code orig} is 'something.else' and {@code start} is
	 * '.' then the return substring is 'else'.
	 *
	 * @param orig
	 *            original string from which we want a substring
	 * @param start
	 *            take everything from the original string after this string
	 * @return a substring from the first character after {@code start} till the
	 *         end of {@code orig}. If {@code orig} does not contain
	 *         {@code start} return the unaltered {@code orig}
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String subAfter(final String orig, final String start) {
		Objects.requireNonNull(orig, NO_NULL_PARAMS);
		Objects.requireNonNull(start, NO_NULL_PARAMS);
		final String subAfter;
		if (orig.contains(start)) {
			subAfter = orig.substring(orig.indexOf(start) + start.length(),
					orig.length());
		} else {
			subAfter = orig;
		}
		return subAfter;
	}

	/**
	 * Gets every characters of {@code orig} before the first occurrence of
	 * string {@code stop}.
	 * <p>
	 * For example when {@code orig} is 'something.else' and {@code stop} is '.'
	 * then the return substring is 'something'.
	 *
	 * @param orig
	 *            the original string from which we want a substring
	 * @param stop
	 *            take everything from the original string before this string
	 * @return a substring from the start of {@code orig} till {@code stop},
	 *         excluding. If {@code orig} does not contain {@code stop} return
	 *         the unaltered {@code orig}
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String subBefore(final String orig, final String stop) {
		Objects.requireNonNull(orig, NO_NULL_PARAMS);
		Objects.requireNonNull(stop, NO_NULL_PARAMS);
		final String subBefore;
		if (orig.contains(stop)) {
			subBefore = orig.substring(0, orig.indexOf(stop));
		} else {
			subBefore = orig;
		}

		return subBefore;
	}

	/**
	 * Gets the substring between the strings {@code afterThis} and
	 * {@code beforeThat}.
	 * <p>
	 * If either {@code afterThis} or {@code beforeThat} don't exist in
	 * {@code orig}, {@code orig} is returned.
	 *
	 * @param src
	 *            the original string from which we want a substring
	 * @param afterThis
	 *            the substring begins after this string
	 * @param beforeThat
	 *            the substring ends before this string
	 * @return the substring between {@code afterThis} and {@code beforeThat} or
	 *         the original string if the original string doesn't contain both
	 *         of those strings
	 * @throws NullPointerException
	 *             if any input string is null
	 */
	public static String subBetween(final String src, final String afterThis,
			final String beforeThat) {
		Objects.requireNonNull(src, NO_NULL_PARAMS);
		Objects.requireNonNull(afterThis, NO_NULL_PARAMS);
		Objects.requireNonNull(beforeThat, NO_NULL_PARAMS);
		final String result;
		if (src.contains(afterThis) && src.contains(beforeThat)) {

			final int afterThisIndex = src.indexOf(afterThis)
					+ afterThis.length();
			final int beforeThisIndex = src.indexOf(beforeThat, afterThisIndex);
			result = src.substring(afterThisIndex, beforeThisIndex);
		} else {
			result = src;
		}
		return result;
	}

	/**
	 * Turns a collection of any objects into a list of strings by calling
	 * {@link String#valueOf(Object)} on each of them.
	 *
	 * @param objects
	 *            objects to convert
	 * @return a {@link List} of strings
	 * @throws NullPointerException
	 *             if {@code objects} is null
	 */
	public static List<String> toStrings(final Collection<?> objects) {
		Objects.requireNonNull(objects, NO_NULL_PARAMS);
		final List<String> strings = new ArrayList<>(objects.size());
		for (final Object obj : objects) {
			strings.add(String.valueOf(obj));
		}
		return strings;
	}

	/**
	 * Turns an array of objects into a list of strings by calling
	 * {@link String#valueOf(Object)} on each of them.
	 *
	 * @param objects
	 *            objects to convert
	 * @return a {@link List} of strings
	 * @throws NullPointerException
	 *             if {@code objects} is null
	 */
	public static List<String> toStrings(final Object[] objects) {
		return toStrings(Arrays.asList(objects));
	}
}
