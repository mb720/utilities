/**
 * Classes and utilities pertaining to string searching and manipulation.
 * @author Matthias Braun
 *
 */
package eu.bges.strings;