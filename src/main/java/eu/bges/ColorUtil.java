/*
 * Copyright 2014 Matthias Braun
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.graphics.RGB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import eu.bges.strings.StringUtil;

/**
 * Utility for handling and converting colors in their different formats like
 * plaintext, RGB, and hexadecimal.
 * <p>
 * Colors in <a
 * href="http://www.w3schools.com/html/html_colornames.asp">plaintext</a> ,
 * hexadecimal format (six figures) and RGB are recognized.
 *
 * @author Matthias Braun
 *
 */
public final class ColorUtil {

    private static final String HASH = "#";

    /**
     * Defines the range of legal RGB values.
     */
    private static final int RGB_LOWER_LIMIT = 0, RGB_UPPER_LIMIT = 255;

    /**
     * Number of values in an red, green and blue value.
     */
    private static final int RGB_VAL_COUNT = 3;

    /**
     * File containing the <a
     * href="http://www.w3schools.com/html/html_colornames.asp">140 specified
     * HTML colors</a> in plaintext with their hexadecimal representation.
     */
    private static final String COLOR_FILE = "/colors.txt";

    /**
     * Matches strings like "#FF00AA" which represent a color in a six-figure
     * hexadecimal format.
     */
    private static final Pattern HEX_COLOR_REGEX = Pattern.compile(HASH
            + MathUtil.HEX_REGEX + "{6}");

    /**
     * A map from the 140 color names <i>in lowercase</i> that are defined in
     * the HTML and CSS color specification to their corresponding hexadecimal
     * value.
     *
     * @see <a
     *      href="http://www.w3schools.com/html/html_colornames.asp">Specified
     *      colors</a>
     */
    private static final Map<String, String> COLOR_NAME_TO_HEX = new HashMap<>();

    /**
     * A multimap from the hex value of the 140 color that are defined in the
     * HTML and CSS color specification to their corresponding plaintext names
     * in english. This is a multimap because the colors cyan and aqua have the
     * same hex value.
     *
     * @see <a
     *      href="http://www.w3schools.com/html/html_colornames.asp">Specified
     *      colors</a>
     */
    private static final Multimap<String, Object> HEX_TO_COLOR_NAME = ArrayListMultimap
            .create();
    private static final Logger LOG = LoggerFactory.getLogger(ColorUtil.class);

    /**
     * This pattern matches name/hexadecimal pairs like "{@code black #000000}"
     * or "{@code red  #FF0000}" and remembers both the color name and the hex
     * number. Used to parse the {@link #COLOR_FILE}.
     */
    private static final Pattern NAME_HEX_REGEX = Pattern
            .compile("(\\w+)\\s+(#.+)");

    /**
     * Used to parse three figure decimals from a string containing an RGB
     * color. They can be separated by whitespace, commas or semicolons.
     * <p>
     * Note that also negative values are parsed so that an invalid, negative
     * value is not converted to a valid, positive one.
     * <p>
     * Examples for recognized strings:
     * <ul>
     * <li>{@code 12 121 0}</li>
     * <li>{@code 12;121;0}</li>
     * <li>{@code 12,121,0}</li>
     * <li>{@code rgb(12,121,0)}</li>
     * <li>{@code -12,121,0} (note that this is not valid RGB, but the minus
     * sign is parsed too to avoid converting an invalid RGB into a valid one)</li>
     * </ul>
     */
    public static final Pattern RGB_REGEX = Pattern
            .compile("(-?\\d{1,3})([,;)\\s}]|$)");

    /**
     * {@link ColorUtil#hexToPlain(String)} returns this if it couldn't convert
     * a color to plaintext.
     */
    public static final String UNKNOWN_COLOR = "unknown color";

    /** This is a utility class not meant to be instantiated by others. */
    private ColorUtil() {
    }

    /**
     * Converts a string into a six figure hexadecimal (e.g., {@code #DABBAD})
     * if possible.
     * <p>
     * The string can be a color in <a
     * href="http://www.w3schools.com/html/html_colornames.asp">plaintext
     * English</a>, or consist of an {@link ColorUtil#RGB_REGEX RGB
     * representation}.
     *
     * @param str
     *            the string that might be a color
     * @return the color's hexadecimal representation wrapped in an
     *         {@link Optional} in case the string could not be converted
     */
    public static Optional<String> anyColorToHex(final String str) {
        String result;
        final Optional<RGB> color = anyColorToRgb(str);
        if (color.isPresent()) {
            result = rgbToHex(color.get());
        } else {
            LOG.warn("Could not cast to hex: " + str);
            result = null;
        }
        return Optional.fromNullable(result);
    }

    /**
     * Converts a string into an {@link RGB} if the string represents a color.
     * <p>
     * The string can be a color in <a
     * href="http://www.w3schools.com/html/html_colornames.asp">plaintext
     * English</a>, in a six figure hexadecimal (e.g., {@code #DABBAD}) or
     * consist of an {@link ColorUtil#RGB_REGEX RGB representation}.
     *
     * @param str
     *            string being converted to an {@link RGB}
     * @return an {@link RGB} wrapped in an {@link Optional} because the
     *         conversion can fail if the string does not represent a color
     */
    public static Optional<RGB> anyColorToRgb(final String str) {
        Optional<RGB> color;
        final ColorType type = getType(str);
        switch (type) {
        case RGB:
            color = rgbStringToRgb(str);
            break;
        case HEX:
            color = hexToRgb(str);
            break;
        case PLAINTEXT:
            color = plainToRgb(str);
            break;
        default:
            color = Optional.absent();
            break;
        }
        return color;
    }

    /**
     * Gets the type of the color (RGB, hex, plaintext).
     *
     * @param str
     *            string representing a color
     * @return type of the color or {@link ColorType#UNKNOWN} if the color in
     *         the string was not found
     */
    public static ColorType getType(final String str) {
        ColorType t;
        if (isHexColor(str)) {
            t = ColorType.HEX;
        } else if (isPlaintextColor(str)) {
            t = ColorType.PLAINTEXT;

        } else if (isRgb(str)) {
            t = ColorType.RGB;

        } else {
            t = ColorType.UNKNOWN;
        }
        return t;
    }

    /**
     * Gets the English name of a color from its hexadecimal representation.
     *
     * @param hexColor
     *            hexadecimal value of the color
     * @return a list of English names. It's empty if the name is not
     *         recognized. There might be multiple names returned because there
     *         exist more than one name for the same color (e.g., cyan and
     *         aqua).
     * @see <a
     *      href="http://www.w3schools.com/html/html_colornames.asp">Recognized
     *      colors</a>
     */
    public static List<String> hexToPlain(final String hexColor) {
        final Multimap<String, Object> map = getHexToColorNameMap();
        final Collection<Object> namesAsObjects = map.get(hexColor);
        return StringUtil.toStrings(namesAsObjects);
    }

    /**
     * Converts a string that represents a hexadecimal color (like
     * {@code #DABDAB}) into an {@link RGB}.
     *
     * @param hexCol
     *            string representing the color in hexadecimal
     * @return the color as an {@link RGB} wrapped in an {@link Optional} in
     *         case the conversion failed
     */
    public static Optional<RGB> hexToRgb(final String hexCol) {
        RGB rgb = null;
        if (isHexColor(hexCol)) {

            final List<Integer> rgbVals = new ArrayList<>();
            // Matches two-figure hex numbers
            final Pattern hexPat = Pattern.compile(MathUtil.HEX_REGEX + "{2}");
            final Matcher m = hexPat.matcher(String.valueOf(hexCol));
            while (m.find()) {
                // Convert from hex to decimal
                final int val = Integer.valueOf(m.group(), 16);
                rgbVals.add(val);
            }
            if (rgbVals.size() == RGB_VAL_COUNT) {
                rgb = new RGB(rgbVals.get(0), rgbVals.get(1), rgbVals.get(2));

            } else {
                LOG.warn(hexCol
                        + " seems to be a color but could not be converted to RGB");
            }
        }
        return Optional.fromNullable(rgb);
    }

    /**
     * Does this object represent a color?
     * <p>
     * Colors in <a
     * href="http://www.w3schools.com/html/html_colornames.asp">plain
     * English</a>, hexadecimal format ({@code #DABDAB}), {@link RGB}, and
     * {@link ColorUtil#RGB_REGEX RGB string representations} are recognized.
     *
     * @param obj
     *            object that might be a color
     * @return whether the object is a color
     */
    public static boolean isColor(final Object obj) {
        return anyColorToRgb(String.valueOf(obj)).isPresent();
    }

    /**
     * Checks whether {@code str} represents a six figure hexadecimal color like
     * {@code #FF00AA}.
     *
     * @param str
     *            string that might represent a color in hexadecimal
     * @return whether this string is a hex color with six figures
     * @see #HEX_COLOR_REGEX
     *
     */
    public static boolean isHexColor(final String str) {
        final Matcher m = HEX_COLOR_REGEX.matcher(str);
        return m.matches();
    }

    /**
     * Checks if the string is one of the 140 specified color names recognized
     * by all browsers.
     *
     * @param str
     *            string to check
     * @return whether the object is a recognized color in English
     * @see <a
     *      href="http://www.w3schools.com/html/html_colornames.asp">Recognized
     *      colors</a>
     */
    public static boolean isPlaintextColor(final String str) {
        return plainToHex(str).isPresent();
    }

    /**
     * Checks whether {@code str} contains an {@link RGB} color.
     *
     * @param str
     *            string that might contain red, green and blue values
     * @return whether the string contains an RGB color
     * @see ColorUtil#rgbStringToRgb(String)
     */
    public static boolean isRgb(final String str) {
        return rgbStringToRgb(str).isPresent();
    }

    /**
     * Converts a color in English into its hexadecimal form. Case insensitive.
     *
     * @param plain
     *            the name of the color in plain English
     * @return the color's hexadecimal form wrapped in an {@link Optional} in
     *         case {@code plain} is not one of the official <a
     *         href="http://www.w3schools.com/html/html_colornames.asp">HTML
     *         colors</a>
     */
    public static Optional<String> plainToHex(final String plain) {

        // The names are in lower case in the map
        final String lowStr = plain.toLowerCase(Locale.ENGLISH);
        final String hex = getColorNameToHexMap().get(lowStr);
        return Optional.fromNullable(hex);
    }

    /**
     * Converts an English plaintext color like 'green' into its {@link RGB}
     * equivalent.
     *
     * @param plain
     *            the color in plaintext English
     *
     * @return the color's {@link RGB} form wrapped in an {@link Optional} in
     *         case {@code plain} is not one of the official <a
     *         href="http://www.w3schools.com/html/html_colornames.asp">HTML
     *         colors</a>
     */
    public static Optional<RGB> plainToRgb(final String plain) {
        Optional<RGB> color;
        final Optional<String> hexColor = plainToHex(plain);
        if (hexColor.isPresent()) {
            color = hexToRgb(hexColor.get());
        } else {
            LOG.warn(
                    "{} is a plaintext color but could not be converted to hex",
                    plain);
            color = Optional.absent();
        }
        return color;
    }

    /**
     * Extracts an {@link RGB} from its string representation.
     * <p>
     * See {@link #RGB_REGEX} for parsable strings.
     *
     * @param str
     *            parse the RGB from this string
     * @return an {@link RGB} if the extraction was successful or an empty
     *         {@link Optional}
     */
    public static Optional<RGB> rgbStringToRgb(final String str) {
        RGB rgb = null;
        final List<Integer> colorVals = new ArrayList<>();
        final Matcher m = RGB_REGEX.matcher(str);
        while (m.find()) {
            final String g = m.group(1);
            final int colorVal = Integer.parseInt(g);
            if (colorVal >= RGB_LOWER_LIMIT && colorVal <= RGB_UPPER_LIMIT) {
                colorVals.add(colorVal);
            }
        }
        if (colorVals.size() == RGB_VAL_COUNT) {
            rgb = new RGB(colorVals.get(0), colorVals.get(1), colorVals.get(2));
        }
        return Optional.fromNullable(rgb);
    }

    /**
     * Converts the red, green, and blue component of a color into a hexadecimal
     * representation (six figures) with a leading hash.
     * <p>
     * Example: {11, 8, 0} → #0B0800
     *
     * @param r
     *            red
     * @param g
     *            green
     * @param b
     *            blue
     * @return the padded hexadecimal representation of the color
     */
    public static String rgbToHex(final int r, final int g, final int b) {
        // The hex numbers are padded to have two digits
        final String redHex = MathUtil.decToPaddedHex(r, 2);
        final String greenHex = MathUtil.decToPaddedHex(g, 2);
        final String blueHex = MathUtil.decToPaddedHex(b, 2);

        return HASH + redHex + greenHex + blueHex;
    }

    /**
     * Converts an {@link RGB} to its hexadecimal representation.
     *
     * @param rgb
     *            color as an {@link RGB}
     * @return the color in hex format
     */
    public static String rgbToHex(final RGB rgb) {
        return rgbToHex(rgb.red, rgb.green, rgb.blue);
    }

    /**
     *
     * @return map from plaintext color names to their hex value. Loaded lazily.
     */
    private static Map<String, String> getColorNameToHexMap() {
        if (COLOR_NAME_TO_HEX.isEmpty()) {
            loadColorsFromFile();
        }
        return COLOR_NAME_TO_HEX;
    }

    /**
     *
     * @return Multimap from to the hex values of colors to their plaintext
     *         names in english. It's a multimap because there are multiple
     *         names describing the same color (e.g., cyan, aqua)
     */
    private static Multimap<String, Object> getHexToColorNameMap() {
        if (HEX_TO_COLOR_NAME.isEmpty()) {
            for (final Entry<String, String> entry : getColorNameToHexMap()
                    .entrySet()) {
                final String colorPlaintext = entry.getKey();
                final String hex = entry.getValue();
                HEX_TO_COLOR_NAME.put(hex, colorPlaintext);
            }
        }
        return HEX_TO_COLOR_NAME;
    }

    /**
     * Loads a mapping from plaintext color names to their hexadecimal
     * representation from a file containing that information.
     */
    private static void loadColorsFromFile() {
        final File colorFile = IOUtil
                .getResource(COLOR_FILE, ColorUtil.class);

        Map<String, String> map;
        if (colorFile.exists()) {
            map = IOUtil.parseVarsFromFile(colorFile, NAME_HEX_REGEX);
        } else {
            LOG.warn("Could not load color file {}", COLOR_FILE);
            map = new HashMap<>();
        }
        COLOR_NAME_TO_HEX.putAll(map);
    }

    /**
     * Types of supported color representations.
     *
     * @author Matthias Braun
     *
     */
    public enum ColorType {
        HEX, PLAINTEXT, RGB, UNKNOWN
    }
}
