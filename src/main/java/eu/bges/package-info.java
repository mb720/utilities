/**
 * All kinds of reusable utility functions for Java.
 * @author Matthias Braun
 *
 *
 */
@NonNullByDefault
package eu.bges;

import org.eclipse.jdt.annotation.NonNullByDefault;

