/**
 * Custom data containers live here that do not belong to a specific domain (PDF, graphics, Colors, etc).
 * @author Matthias Braun
 *
 */
package eu.bges.data;