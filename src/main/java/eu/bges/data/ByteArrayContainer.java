/*
 * Copyright 2014 Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges.data;

import java.io.Serializable;

/**
 * Abstract super class for byte array wrapper types
 *
 * @author Martin Gangl
 */
public abstract class ByteArrayContainer implements Serializable {

    // Serialization support
    private static final long serialVersionUID = 1L;

    // Vars
    protected final int MIN_DESCRIPTION_LENGTH = 5;
    private String description;

    /**
     * @return the byteArray
     */
    public abstract byte[] getByteArray();

    /**
     * @return the description of the containers element
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return true if and only if the contained byte array is not null and has
     *         at least one element and if the description is at least of length
     *         5
     */
    public abstract boolean isValid();

    /**
     * @param description
     *            for this containers element
     */
    public void setDescription(final String description) {
        if (description == null) {
            this.description = "";
        } else {
            this.description = description;
        }
    }
}