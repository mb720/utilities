/*
 * Copyright 2014 Matthias Braun, Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Constants and utility functions for dealing with time and date.
 *
 * @author Matthias Braun
 * @author Martin Gangl
 *
 */
public final class TimeUtil {

    private static final Logger LOG = LoggerFactory.getLogger(TimeUtil.class);
    /**
     * The format of a combined date and time representation according to ISO
     * 8601. The delimiter between date and time is omitted.
     *
     * @see <a
     *      href="https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations">ISO
     *      8601</a>
     *
     */
    public static final String DATE_TIME_FORMAT_ISO_8601 = "yyyy-MM-dd HH:mm:ssZZ";

    private static final int DEFAULT_MIN_YEAR = 1899;

    private static final int DEFAULT_MAX_YEAR = 2199;

    private static int minYear = DEFAULT_MIN_YEAR;
    private static int maxYear = DEFAULT_MAX_YEAR;

    /**
     * The day month year pattern without the distance from UTC
     */
    public static final String DEFAULT_DATE_PATTERN = "dd.MM.yyyy";

    /** This is a utility class not meant to be instantiated by others. */
    private TimeUtil() {
    }

    /**
     * @param date
     *            original date (for example 2011-08-15)
     * @param days
     *            to be added (for example 5)
     * @return date with the added days (for example 2011-08-20)
     */
    public static Date addDaysToDate(final Date date, final long days) {
        return new Date(date.getTime() + days * 86400000L); // days*24h*60min*60sec*1000msec
    }

    /**
     * @param year
     *            of the date to be created
     * @param month
     *            of the date to be created
     * @param day
     *            of the date to be created
     * @return a comparable SQL compatible Date object focusing only on the
     *         date, ignoring the time
     * @throws IllegalArgumentException
     *             if illegal date values have been given according to
     *             {@link #isDateValid(int, int, int)}
     */
    public static Date getDate(final int year, final int month, final int day) {
        if (isDateValid(year, month, day)) {
            final Calendar cal = Calendar.getInstance();
            cal.set(year, month, day);
            return new Date(cal.getTimeInMillis());
        }
        throw new IllegalArgumentException(
                "Given date parameters out of range " + day + "." + month + "."
                        + year + ")");
    }

    /**
     * @param startDate
     *            for the calculation
     * @param endDate
     *            for the calculation
     * @return the difference in days between the two dates; returns 0 if one or
     *         both dates are null
     */
    public static long getDayDifferenceBetweenDates(final Date startDate,
            final Date endDate) {
        if (startDate == null || endDate == null) {
            return 0;
        }
        return (endDate.getTime() - startDate.getTime()) / 1000L / 3600L / 24L;
    }

    /**
     * Gets the hour and the minute from some {@code time}.
     *
     * @param time
     *            object to be converted
     * @return an integer array [hour, minute] with the corresponding values if
     *         and only if the provided time object is valid; [-1, -1] will be
     *         returned otherwise
     */
    public static int[] getHourMinuteFromTime(final Time time) {
        final int[] data = { -1, -1 };
        if (time != null) {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(time);
            final int hour = cal.get(Calendar.HOUR_OF_DAY);
            final int minute = cal.get(Calendar.MINUTE);
            if (isTimeValid(hour, minute)) {
                data[0] = hour;
                data[1] = minute;
            }
        }
        return data;
    }

    /**
     * @param hour
     *            of the time to be created
     * @param minute
     *            of the time to be created
     * @return a comparable SQL compatible Time object focusing only on the hour
     *         and the minute, ignoring the (milli)seconds
     * @throws IllegalArgumentException
     *             if {@code hour} and {@code minute} are not valid according to
     *             {@link #isTimeValid(int, int)}
     */
    public static Time getTime(final int hour, final int minute) {
        if (isTimeValid(hour, minute)) {
            final Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return new Time(cal.getTimeInMillis());
        }
        throw new IllegalArgumentException("Given time parameters out of range");
    }

    /**
     * Parses the {@link Time} from a string.
     *
     * @param s
     *            time string to be converted
     * @return a SQL time object or null if a null string has been given
     * @throws IllegalArgumentException
     *             if an error occurs
     * @throws NumberFormatException
     *             if an error occurs
     */
    public static Time getTimeFromString(final String s)
            throws IllegalArgumentException, NumberFormatException,
            StringIndexOutOfBoundsException {
        if (s != null) {
            s.trim();
            final int hour = Integer.parseInt(s.substring(0, s.indexOf(":")));
            final int min = Integer.parseInt(s.substring(s.indexOf(":") + 1,
                    s.length()));
            return getTime(hour, min);
        }
        return null;
    }

    /**
     * @param date
     *            object to be converted
     * @return an integer array [year, month, date] with the corresponding
     *         values if and only if the provided date object is valid; [-1, -1,
     *         -1] will be returned otherwise
     */
    public static int[] getYearMonthDayFromDate(final Date date) {
        final int[] data = { -1, -1, -1 };
        if (date != null) {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            final int year = cal.get(Calendar.YEAR);
            final int month = cal.get(Calendar.MONTH);
            final int day = cal.get(Calendar.DAY_OF_MONTH);
            if (isDateValid(year, month, day)) {
                data[0] = year;
                data[1] = month;
                data[2] = day;
            }
        }
        return data;
    }

    /**
     * @param year
     *            of the date to be checked
     * @param month
     *            of the date to be checked, zero-indexed
     * @param day
     *            of the date to be checked
     * @return true if and only if the date is valid (month between 1 and 12,
     *         day according to maximum days of the given month, year between
     *         minimum and maximum bounds
     */
    public static boolean isDateValid(final int year, final int month,
            final int day) {
        if (year < minYear || year > maxYear) {
            return false;
        }
        if (month < 0 || month > 11) {
            return false;
        }
        if (day < 1 || day > 31) {
            return false;
        }

        // Check every month that has less than 31 days
        switch (month) {
        case 1:
            // Consider leap years where there is a 29th of February
            if (day > 29) {
                return false;
            }
            break;
        case 3:
            if (day > 30) {
                return false;
            }
            break;
        case 5:
            if (day > 30) {
                return false;
            }
            break;
        case 8:
            if (day > 30) {
                return false;
            }
            break;
        case 10:
            if (day > 30) {
                return false;
            }
            break;
        default:
            LOG.warn("Unknown month with number {}", month);
        }
        return true;
    }

    /**
     * @param hour
     *            of the time to be checked
     * @param minute
     *            of the time to be checked
     * @return true if and only if the time is valid (hours between 0 and 23,
     *         minutes between 0 and 59)
     */
    public static boolean isTimeValid(final int hour, final int minute) {
        return !(hour < 0 || hour > 23 || minute < 0 || minute > 59);
    }

    /**
     * @param year
     *            to be set as maximum year that will be accepted
     * @return true if the year is at least as big as the default maximum year
     */
    public static boolean setMaxYear(final int year) {
        if (year < DEFAULT_MAX_YEAR) {
            return false;
        }
        maxYear = year;
        return true;
    }

    /**
     * @param year
     *            to be set as minimum year that will be accepted
     * @return true if the year is at least as small as the default minimal year
     */
    public static boolean setMinYear(final int year) {
        if (year > DEFAULT_MIN_YEAR) {
            return false;
        }
        minYear = year;
        return true;
    }

    /**
     * Converts a time stamp to the corresponding date and time string.
     * <p>
     * Timezone is the local time.
     *
     * @param timeStamp
     *            milliseconds since 00:00:00 UTC, Thursday, 1 January 1970
     * @return date and time as a string in the format
     *         {@value #DATE_TIME_FORMAT_ISO_8601}
     *
     */
    public static String toLocal(final long timeStamp) {
        final DateTime time = new DateTime(timeStamp);
        final DateTimeFormatter formatter = DateTimeFormat.forPattern(
                DATE_TIME_FORMAT_ISO_8601).withLocale(Locale.getDefault());
        return time.toString(formatter);
    }

    /**
     * Converts a time stamp to the corresponding date and time string.
     * <p>
     * Timezone is UTC.
     *
     * @param timeStamp
     *            milliseconds since 00:00:00 UTC, Thursday, 1 January 1970
     * @return date and time as a string in the format
     *         {@value #DATE_TIME_FORMAT_ISO_8601}
     *
     */
    public static String toUtc(final long timeStamp) {
        final DateTime time = new DateTime(timeStamp);
        final DateTimeFormatter formatter = DateTimeFormat.forPattern(
                DATE_TIME_FORMAT_ISO_8601).withZoneUTC();
        return time.toString(formatter);
    }
}
