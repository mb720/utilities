/*
 * Copyright 2014 Matthias Braun
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges;

import java.util.Locale;

/**
 * Provides utilities for dealing with different locales and
 * internationalization in general.
 *
 * @author Matthias Braun
 *
 */
public final class LocaleUtil {

    /**
     * Sets the current locale.
     *
     * @param tag
     *            the {@link LangTag} identifying the locale to be set
     */
    public static void setLocaleTo(final LangTag tag) {
        final String tagStr = tag.toString();
        final Locale newLocale = Locale.forLanguageTag(tagStr);
        Locale.setDefault(newLocale);
    }

    /**
     * Tags for different languages.
     *
     * @author Matthias Braun
     * @see <a
     *      href="http://www.oracle.com/technetwork/java/javase/javase7locales-334809.html">Locales</a>
     *
     */
    public enum LangTag {

        ROMANIAN("ro-RO"), SWEDISH("sv-SE"), ARABIC_IRAQ("ar-IQ"), ARABIC_SAUDI_ARABIA(
                "ar_SA"), TURKISH("tr-TR"), AUSTRIAN("de-AT"), GERMAN("de-DE"), UK(
                        "en-GB"), US("en-US"), SLOVENIAN("sl-SI");

        private final String tag;

        LangTag(final String tag) {
            this.tag = tag;
        }

        @Override
        public String toString() {
            return this.tag;
        }

    }
}
