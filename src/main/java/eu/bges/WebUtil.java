/*
 * Copyright 2014 Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility classes related to web technologies like Email and PHP.
 *
 * @author Martin Gangl
 *
 */
public final class WebUtil {
	private static final Logger LOG = LoggerFactory.getLogger(WebUtil.class);

	/**
	 * Executes a PHP command.
	 *
	 * @param phpUrl
	 *            url of the host
	 * @param parameters
	 *            of the command
	 * @return the command's return value
	 * @throws IOException
	 *             if the connection to the host can't be opened
	 */
	public static String executePHP(final String phpUrl,
			final List<String[]> parameters) throws IOException {
		String retVal = null;
		final URL hostUrl = new URL(phpUrl);
		final URLConnection urlConnection = hostUrl.openConnection();
		urlConnection.setDoOutput(true);
		if (phpUrl != null) {
			try (final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					urlConnection.getOutputStream());
					final InputStream in = urlConnection.getInputStream();) {
				final StringBuilder postData = new StringBuilder();
				if (parameters != null && parameters.size() > 0) {
					for (final String[] curParam : parameters) {
						if (curParam.length >= 2 && curParam[0] != null
								&& curParam[1] != null) {
							if (postData.length() > 0) {
								postData.append("&");
							}
							final String utf8 = IOUtil.UTF_8.toString();
							postData.append(URLEncoder
									.encode(curParam[0], utf8));
							postData.append("=");
							postData.append(URLEncoder
									.encode(curParam[1], utf8));
						}
					}
				}

				outputStreamWriter.write(postData.toString());
				outputStreamWriter.flush();
				outputStreamWriter.close();
				int c = 0;
				final StringBuilder incoming = new StringBuilder();
				while (c >= 0) {
					c = in.read();
					incoming.append((char) c);
				}
				if (incoming.length() > 1) {
					retVal = incoming.substring(0, incoming.length() - 1);
				} else {
					retVal = incoming.toString();
				}
			} catch (final UnsupportedEncodingException e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		return retVal;
	}

	/**
	 * @param email
	 *            address(es) of the primary receiver(s) (divided by comma!)
	 * @param subject
	 *            of the message
	 * @param cc
	 *            address(es) of the cc receiver(s) (divided by comma!)
	 * @param bcc
	 *            address(es) of the bcc receiver(s) (divided by comma!)
	 * @param body
	 *            content of the message
	 * @return the mailto URI corresponding to the given input values
	 * @throws IllegalArgumentException
	 *             if illegal or incomplete arguments have been given
	 */
	public static URI getMailtoUri(final String email, final String subject,
			final String cc, final String bcc, final String body)
					throws IllegalArgumentException {
		// Check input parameters
		if (email == null || email.indexOf("@") == -1) {
			throw new IllegalArgumentException();
		}
		if (cc != null && cc.length() > 0 && cc.indexOf("@") == -1) {
			throw new IllegalArgumentException();
		}
		if (bcc != null && bcc.length() > 0 && bcc.indexOf("@") == -1) {
			throw new IllegalArgumentException();
		}

		// Create URI ssp
		final StringBuilder ssp = new StringBuilder(email);
		if (subject != null && subject.length() > 0) {
			ssp.append("?subject=" + subject);
		}
		if (cc != null && cc.length() > 0) {
			ssp.append("&cc=" + cc);
		}
		if (bcc != null && bcc.length() > 0) {
			ssp.append("&bcc=" + bcc);
		}
		if (body != null && body.length() > 0) {
			ssp.append("&body=" + body);
		}

		// Create and return the URI
		try {
			return new URI("mailto", ssp.toString(), null);
		} catch (final URISyntaxException e) {
			throw new IllegalArgumentException();
		}
	}

}
