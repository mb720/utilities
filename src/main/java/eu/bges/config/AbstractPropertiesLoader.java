/*
 * Copyright 2014 Matthias Braun
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

/**
 * Provides methods to load a properties file in the
 * {@link Properties#load(java.io.Reader) Eclipse property format} and read
 * values from it.
 * <p>
 * Extending classes must provide the properties file.
 *
 * @author Matthias Braun
 *
 */
public abstract class AbstractPropertiesLoader {

    private static final Logger LOG = LoggerFactory
            .getLogger(AbstractPropertiesLoader.class);
    /**
     * If a key points to a list of values in the properties file, this is the
     * string that is assumed to separate the individual values if no other
     * string is given.
     */
    public static final String DEFAULT_SEPARATOR = ",";

    /**
     * List of properties.
     */
    private final Properties properties = new Properties();

    /**
     * Whether the property file is loaded
     */
    private boolean propFileIsLoaded = false;

    /**
     * Gets the file containing the properties.
     *
     * @return the properties file
     */
    public abstract File getPropertiesFile();

    /**
     * Gets a list of values from a {@code key} in the properties file.
     * <p>
     * The individual values are assumed to be separated by
     * {@value #DEFAULT_SEPARATOR}.
     *
     * @param key
     *            the key to the values in the file
     * @return a list of values from the properties file
     * @throws KeyNotFoundException
     *             if the {@code key} is not found in the file
     */
    protected List<String> getList(final String key)
            throws KeyNotFoundException {
        return getList(key, DEFAULT_SEPARATOR);
    }

    /**
     * Gets a list of values from a {@code key} in the properties file.
     *
     * @param key
     *            he key to the values in the file
     * @param separator
     *            the string separating the individual values (a comma or a
     *            semicolon for example)
     * @return a list of values from the properties file
     * @throws KeyNotFoundException
     *             Thrown when the {@code key} is not found in the file
     */
    protected List<String> getList(final String key, final String separator)
            throws KeyNotFoundException {
        final String value = getValue(key);

        return Splitter.on(separator).splitToList(value);
    }

    /**
     * Gets the property's value using its {@code key}.
     *
     * @param key
     *            the name of the property
     * @return the property's value or null
     * @throws NullPointerException
     *             if {@code key} is null
     * @throws KeyNotFoundException
     *             if {@code key} was not found in the properties file
     */
    protected String getValue(final String key) throws KeyNotFoundException {
        if (!propFileIsLoaded) {
            load(getPropertiesFile());
        }
        final String value = properties.getProperty(key);
        if (value == null) {
            throw new KeyNotFoundException(key, getPropertiesFile());
        } else {
            return value;
        }
    }

    /**
     * Initialize this object by loading the properties file.
     *
     * @param propFile
     *            the {@code File} containing the properties
     */
    private void load(final File propFile) {
        try (InputStream stream = new FileInputStream(propFile)) {
            properties.load(stream);
            propFileIsLoaded = true;
        } catch (final IOException e) {
            LOG.warn("Could not load property file {}", propFile, e);
        }
    }

}
