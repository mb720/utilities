/*
 * Copyright 2014 Matthias Braun
 *
 * This class is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This class is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this class. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.bges.config;

import java.io.File;

/**
 * Throw this when you were looking for a key, for example in a config file, but
 * could not find it.
 *
 * @author Matthias Braun
 *
 */
public class KeyNotFoundException extends Exception {

    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a {@link KeyNotFoundException}.
     *
     * @param key
     *            the key that wasn't found
     * @param file
     *            the file which did not contain the {@code key}
     */
    public KeyNotFoundException(final String key, final File file) {
        super(getMessage(key, file));
    }

    /**
     * Create the message that is printed when the {@code KeyNotFoundException}
     * is thrown.
     *
     * @param key
     *            the key that wasn't found in the {@code file}
     * @param file
     *            the file that didn't contain the {@code key}
     * @return message that the {@code key} wasn't found in the {@code file}
     */
    private static final String getMessage(final String key, final File file) {
        final String msg = "Could not find key '" + key + "' in file '"
                + file.getAbsolutePath() + "'";
        return msg;

    }
}
