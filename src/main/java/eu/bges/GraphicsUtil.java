/*
 * Copyright 2014 Matthias Braun, Martin Gangl
 *
 * This library is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.bges;

import java.awt.Image;
import java.io.File;
import java.util.Locale;

import javax.swing.ImageIcon;

/**
 * Utilities and constants for dealing with images and image manipulation.
 *
 * @author Matthias Braun
 * @author Martin Gangl
 */
public final class GraphicsUtil {

	/**
	 * Supported graphic file extensions
	 */
	public static final String[] GRAPHIC_FILE_EXTENSIONS = { "jpg", "jpeg",
			"gif", "png" };

	/**
	 * Checks whether {@code file} is a supported graphics file.
	 * <p>
	 * Note: only the extension of the file is checked, not its size or actual
	 * content
	 *
	 * @param file
	 *            to be checked whether it is a supported graphic file or not
	 * @return true if the file is a supported graphics file
	 */
	public static boolean isGraphicFile(final File file) {
		boolean isSupported = false;
		if (file != null && file.exists()) {
			/*
			 * Use toLowerCase(Locale.ENGLISH): Otherwise 'pic.GIF' would be
			 * unsupported in Turkey
			 */
			final String fileLowerCase = file.getName().toLowerCase(
					Locale.ENGLISH);
			for (final String supportedExt : GRAPHIC_FILE_EXTENSIONS) {
				if (fileLowerCase.endsWith(supportedExt)) {
					isSupported = true;
					break;
				}
			}
		}
		return isSupported;
	}

	/**
	 * Scales an icon to a desired size.
	 *
	 * @param unscaledIcon
	 *            original image in ImageIcon format
	 * @param desiredWidth
	 *            in Pixel, must be greater than zero
	 * @param desiredHeight
	 *            in Pixel, must be greater than zero
	 * @param renderQuality
	 *            (see Image class for further details)
	 * @return an ImageIcon containing the image in the scaled size (scaling
	 *         will be done with respect to the original aspect ratio)
	 * @throws IllegalArgumentException
	 *             if invalid parameters (null image, width/height below 1) are
	 *             given
	 */
	public static ImageIcon scaleImageIcon(final ImageIcon unscaledIcon,
			final int desiredWidth, final int desiredHeight,
			final int renderQuality) throws IllegalArgumentException {
		if (unscaledIcon == null || desiredWidth <= 0 || desiredHeight <= 0) {
			throw new IllegalArgumentException(
					"Image may not be null and desired height/width must be greater than zero");
		}
		if (renderQuality != Image.SCALE_AREA_AVERAGING
				&& renderQuality != Image.SCALE_DEFAULT
				&& renderQuality != Image.SCALE_FAST
				&& renderQuality != Image.SCALE_REPLICATE
				&& renderQuality != Image.SCALE_SMOOTH) {
			throw new IllegalArgumentException(
					"Invalid value for render quality, see Java API for Image class");
		}

		final Image unscaledImg = unscaledIcon.getImage();
		Image scaledImg = null;

		// Determine if image is too big or too small
		if (unscaledImg.getWidth(null) != desiredWidth
				|| unscaledImg.getHeight(null) != desiredHeight) {

			// Image is wider than high
			if (unscaledImg.getWidth(null) > unscaledImg.getHeight(null)) {
				// Determine scale factor of image width and scale it
				double scaleFactor = div(desiredWidth,
						unscaledImg.getWidth(null));
				scaledImg = new ImageIcon(unscaledImg.getScaledInstance(
						(int) (unscaledImg.getWidth(null) * scaleFactor),
						(int) (unscaledImg.getHeight(null) * scaleFactor),
						renderQuality)).getImage();

				/*
				 * Determine if image is still too high and if so scale it down
				 * again
				 */
				if (scaledImg.getHeight(null) > desiredHeight) {
					scaleFactor = div(desiredHeight, scaledImg.getHeight(null));
					scaledImg = scaledImg.getScaledInstance(
							(int) (scaledImg.getWidth(null) * scaleFactor),
							(int) (scaledImg.getHeight(null) * scaleFactor),
							renderQuality);
				}
			}

			// Image is higher than wide
			else {
				// Determine scale factor of image height and scale it
				double scaleFactor = div(desiredHeight,
						unscaledImg.getHeight(null));
				scaledImg = new ImageIcon(unscaledImg.getScaledInstance(
						(int) (unscaledImg.getWidth(null) * scaleFactor),
						(int) (unscaledImg.getHeight(null) * scaleFactor),
						renderQuality)).getImage();

				/*
				 * Determine if image is still too wide and if so scale it down
				 * again
				 */
				if (scaledImg.getWidth(null) > desiredWidth) {
					scaleFactor = div(desiredWidth, scaledImg.getWidth(null));
					scaledImg = scaledImg.getScaledInstance(
							(int) (scaledImg.getWidth(null) * scaleFactor),
							(int) (scaledImg.getHeight(null) * scaleFactor),
							renderQuality);
				}
			}
		}
		return new ImageIcon(scaledImg);
	}

	/**
	 * Divides a {@code dividend} by a {@code divisor}.
	 *
	 * @param dividend
	 *            number that is divided by {@code divisor}
	 * @param divisor
	 *            number that divides {@code dividend}
	 * @return the quotient of {@code dividend} and {@code divisor} as a double
	 */
	private static double div(final int dividend, final int divisor) {
		return MathUtil.div(dividend, divisor).doubleValue();
	}
}
