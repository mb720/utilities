package eu.bges;

import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * Utilities for dealing with money, taxes and other financial matters.
 * <p>
 * Enable strict floating point calculations (in accordance with <a
 * href="https://en.wikipedia.org/wiki/IEEE_754">IEEE 754</a>) to ensure
 * portability between JVMs.
 * */
public strictfp class FinanceUtil {

    /**
     * Converts a double {@code value}to a currency string.
     * <p>
     * The value is rounded to three decimal places.
     * <p>
     * The output is locale-dependent: The decimal separator, the position of
     * the currency symbol and of course the currency symbol itself will vary
     * according to the default locale.
     * <p>
     * Example outputs for different locales with the input value
     * {@code 200_000_000.0505}:
     *
     * <pre>
     * Austria: € 200.000.000,051
     * Germany: 200.000.000,051 €
     * Slovenia: € 200.000.000,051
     * UK: £200,000,000.051
     * US: $200,000,000.051
     * </pre>
     *
     * @param value
     *            to be formated as a currency value
     * @return a string currency representation of the given value
     */
    public static String doubleToCurrency(final double value) {
        final NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMaximumFractionDigits(3);
        // The default rounding mode is HALF_EVEN
        nf.setRoundingMode(RoundingMode.HALF_UP);
        return nf.format(value);
    }

    /**
     * The normal value added tax (VAT) for various countries.
     *
     * @author Matthias Braun
     *
     */
    public enum NormalVat {
        /** Austria */
        AUT(0.20),

        /** Czech Republic */
        CZ(0.21),

        /** Germany */
        GER(0.19),

        /** Hungary */
        HUN(0.27),

        /** Italy */
        ITA(0.22),

        /** Slovakia */
        SK(0.20),

        /** Slovenia */
        SLO(0.22),

        /** Switzerland */
        SWI(0.08);

        private final double vat;

        private NormalVat(final double vat) {
            this.vat = vat;
        }

        public double getVat() {
            return vat;
        }
    }

}
